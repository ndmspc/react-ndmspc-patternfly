import { resolve } from 'path'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/lib/index.jsx'),
      name: 'React Library Vite',
      fileName: (format) => `react-ndmspc-patternfly.${format}.js`
    },
    rollupOptions: {
      // externalize deps that shouldn't be bundled
      // into your library
      external: ['react', 'react-dom', '@textea/json-viewer', '@textea/json-viewer'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          react: 'React',
          rxjs: 'rxjs',
          jsroot: 'jsroot'
        },
        inlineDynamicImports: true
      }
    }
  },
  plugins: [react()],
  optimizeDeps: {
    exclude: ['gl > gl']
  }
})