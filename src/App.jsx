import React from "react";
import { NdmspcApp } from "./lib/components/NdmspcApp";
import { EmptyPlugin, NdmspcBinPlugin } from "./lib/plugins";

export default function App() {
  return (<NdmspcApp plugins={{ NdmspcBinPlugin, EmptyPlugin}} />);
}