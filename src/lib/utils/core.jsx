import { http2root } from '@ndmspc/react-ndmspc-core';

const applyConfigFromAnalysis = (config, analysis, pluginName, ndmspcConfiguration, ndmspcConfigurationStore, applyAnalysis = true) => {

  // console.log("ndmspcConfiguration ", ndmspcConfiguration)
  if (!analysis) return
  const projection = analysis.projections[analysis.projection]
  const plugins = analysis?.plugins ? analysis?.plugins[pluginName] : null;
  let cfg = (config && Object.keys(config).length > 0) ? config : (plugins ? plugins[plugins.current]?.config : null)
  applyPluginConfig(cfg, projection, pluginName, ndmspcConfiguration, ndmspcConfigurationStore, applyAnalysis)
}

const applyPluginConfig = (cfg, projection, pluginName, ndmspcConfiguration, ndmspcConfigurationStore, applyAnalysis = true) => {

  if (!cfg) return
  // TODO rebin
  cfg.ndmspc.cuts = [];
  for (let i = 0; i < projection.axes.length; i++) {
    cfg.ndmspc.cuts.push({
      axis: projection.axes[i],
      bin: { min: 1, max: 1 },
      rebin: 1,
      enabled: true
    })
  }

  let dir = http2root(projection.file)[1];
  dir = dir.substring(0, dir.lastIndexOf('/'));
  cfg.ndmspc.output.host = http2root(projection.file)[0];
  cfg.ndmspc.output.dir = `${dir}/bins`
  cfg.ndmspc.output.file = "content.root";

  let newConfiguration = {}
  if (ndmspcConfiguration) {
    newConfiguration = {
      ...ndmspcConfiguration, [pluginName]: { ...ndmspcConfiguration[pluginName], current: cfg.name, [cfg.name]: { config: cfg } }
    }
  } else {
    newConfiguration = {
      [pluginName]: { current: cfg.name, [cfg.name]: { config: cfg } }

    }
  }

  newConfiguration["applyPlugin"] = true
  newConfiguration["applyAnalysis"] = applyAnalysis
  ndmspcConfigurationStore.setData(newConfiguration);
  console.log(`Sent to plugin '${pluginName}' : `, newConfiguration)
}

export { applyConfigFromAnalysis, applyPluginConfig }


