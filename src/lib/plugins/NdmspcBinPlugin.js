import { AppGlobalScope, Distributor } from "@ndmspc/react-ndmspc-core";
import {
  urlExists,
  getFileObjectNames,
  submitSalsaJob,
} from "@ndmspc/react-ndmspc-core";

const NdmscpBinPlugin = new AppGlobalScope({
  name: "NdmspcBinPlugin",
  configuration: null,
  cluster: null,
  isVR: false,
  useCache: true,
  selectedTools: new Map(),
  tools: {
    bin: {
      title: "Bin selection",
      type: "select",
      tooltip: "Bin selection type",
      value: "single", // only strings
      options: [
        ["single", "all"], // only strings
        ["single", "all"], // only strings
        ["single", "all"], // only strings
      ],
    },
    refresh: {
      title: "Update mapping",
      type: "select",
      icon: "sync",
      value: "dev",
      tooltip: "Forcing main histogram to be updated with upstream",
      options: [
        ["dev", "prod"],
        ["dev", "prod"],
        ["dev", "prod"],
      ],
    },
    configuration: {
      title: "Configuration",
      type: "button",
      value: null,
      tooltip: "Manage configuration",
      options: [[], [], []],
    },
  },

  workspaces: {
    default: [],
  },
});

NdmscpBinPlugin.addDistributor(new Distributor("cluster"));
NdmscpBinPlugin.addDistributor(new Distributor("configuration"));
NdmscpBinPlugin.addDistributor(new Distributor("pluginPipe"));
NdmscpBinPlugin.addDistributor(new Distributor("selectedPluginTool"));
NdmscpBinPlugin.addDistributor(new Distributor("job"));
NdmscpBinPlugin.addDistributor(new Distributor("binObjects"));
// NdmscpBinPlugin.addDistributor(new Distributor("binSelect"));

const handleConfiguration = (data) => {
  NdmscpBinPlugin.state.configuration = data.config;
};
NdmscpBinPlugin.addHandlerFunc("configuration", handleConfiguration, null);
NdmscpBinPlugin.createSubscription("configuration");

const handleClusterChange = (data) => {
  NdmscpBinPlugin.state.cluster = data;
};
NdmscpBinPlugin.addHandlerFunc("cluster", handleClusterChange, null);
NdmscpBinPlugin.createSubscription("cluster");

const handleSlectedPluginTool = (data) => {
  console.log("NdmscpBinPlugin::handleSlectedPluginTool ", data);
  NdmscpBinPlugin.state.selectedTools.set(data.name, data.value);
};
NdmscpBinPlugin.addHandlerFunc(
  "selectedPluginTool",
  handleSlectedPluginTool,
  null
);
NdmscpBinPlugin.createSubscription("selectedPluginTool");

NdmscpBinPlugin.userFunctions = {
  // onBinHover: (data) => {
  //   console.log("NDMSPC NdmscpBinPlugin : onBinHover()", data);
  // },

  onScanHistogram: async (data) => {
    console.log("NDMSPC NdmscpBinPlugin : onScanHistogram()", data);

    const cfg = { ndmspc: { log: {} } };
    cfg["ndmspc"]["log"]["type"] = "always";
    cfg["ndmspc"]["log"]["dir"] =
      "root://eos.ndmspc.io//eos/ndmspc/scratch/ndmspc/logs";

    const { analysis, binPresent = 2, binMissing = 1 } = data;
    const currentProjection = analysis.projections[analysis.projection];

    const clusterInfo = NdmscpBinPlugin.state.cluster;
    // console.log(clusterInfo);
    const selectedTools = NdmscpBinPlugin.state?.selectedTools;
    // console.log(selectedTools);

    const hProjMapDir = `${analysis.base.root}${analysis.base.path}/${currentProjection.name}`;

    const extra = {
      canvas: "workspace-main",
      filename: `${hProjMapDir}/hProjMap.root`,
      bin: "single",
    };

    const commands = [
      `ndmspc _macro '/usr/share/ndmspc/macros/NdmspcScanProjectionMap.C' '"${hProjMapDir}",${binPresent},${binMissing}' `,
    ];
    console.log(commands);

    if (clusterInfo?.executorUrl) {
      // console.log(clusterInfo?.executorUrl);

      await submitSalsaJob(
        clusterInfo?.executorUrl,
        commands,
        clusterInfo.submitUrl,
        cfg
      ).then((res) => {
        console.log(res, extra);
        NdmscpBinPlugin.getDistributorById("job")?.sendOutputData({
          res,
          extra,
        });
      });
    } else {
      console.log(clusterInfo);
      console.log("No executor");
      NdmscpBinPlugin.getDistributorById("job")?.sendOutputData({
        error: "No Executor",
        extra: clusterInfo,
      });
    }
  },

  onBinClick: async (data) => {
    const cfg = NdmscpBinPlugin?.state?.configuration;
    const clusterInfo = NdmscpBinPlugin.state.cluster;
    const selectedTools = NdmscpBinPlugin.state?.selectedTools;
    console.log(
      "NDMSPC NdmscpBinPlugin : onBinClick()",
      data,
      cfg,
      clusterInfo,
      selectedTools
    );

    if (!cfg) {
      console.log("Error: no configuration. ");
      NdmscpBinPlugin.getDistributorById("pluginPipe")?.sendOutputData({
        type: "plugin",
        action: "config",
        payload: { err: "No configuration" },
      });
      return;
    }

    cfg["ndmspc"]["log"]["type"] = "always";
    cfg["ndmspc"]["log"]["dir"] =
      "root://eos.ndmspc.io//eos/ndmspc/scratch/ndmspc/logs";

    const { analysis, obj: histogram } = data;
    const projection = analysis.projections[analysis.projection];
    // console.log(analysis, histogram);
    const bins = [];
    ({ binx: bins[0] = 0, biny: bins[1] = 0, binz: bins[2] = 0 } = data);
    // console.log(bins);

    const hProjMapDir = `${analysis.base.root}${analysis.base.path}/${projection.name}`;
    const binPresent = 2;
    const binMissing = 1;

    const extra = {
      canvas: "workspace-main",
      filename: `${hProjMapDir}/hProjMap.root`,
      bin: selectedTools.get("bin"),
    };

    // TODO fix
    if (histogram._typename.startsWith("TH1") && !data.isVR)
      bins[0] = data.bin + 1;

    // console.log(cfg);
    if (cfg.ndmspc.cuts[0]) {
      cfg.ndmspc.cuts[0].bin.min = bins[0];
      cfg.ndmspc.cuts[0].bin.max = bins[0];
    }
    if (cfg.ndmspc.cuts[1]) {
      cfg.ndmspc.cuts[1].bin.min = bins[1];
      cfg.ndmspc.cuts[1].bin.max = bins[1];
    }
    if (cfg.ndmspc.cuts[2]) {
      cfg.ndmspc.cuts[2].bin.min = bins[2];
      cfg.ndmspc.cuts[2].bin.max = bins[2];
    }

    let cmd_params = "";
    const job_inputs = cfg.ndmspc?.job?.inputs;
    if (job_inputs) {
      for (let i = 0; i < job_inputs.length; i++) {
        cmd_params += `"${job_inputs[i]}" `;
      }
    }

    const cmd_base = `ndmspc _macro '/usr/share/ndmspc/macros/NdmspcPointRun.C' '"ndmspc.json",true' ${cmd_params}`;
    // console.log("NdmscpBinPlugin: ", cmd_base);

    let f;
    if (histogram._typename.startsWith("TH1")) {
      f = `${analysis.base.http}/${analysis.base.path}/${projection.name}/bins/${bins[0]}/content.root`;
    } else if (histogram._typename.startsWith("TH2")) {
      f = `${analysis.base.http}/${analysis.base.path}/${projection.name}/bins/${bins[0]}/${bins[1]}/content.root`;
    } else if (histogram._typename.startsWith("TH3")) {
      f = `${analysis.base.http}/${analysis.base.path}/${projection.name}/bins/${bins[0]}/${bins[1]}/${bins[2]}/content.root`;
    } else {
      console.log("Unsuported histogram ", histogram);
      return;
    }

    NdmscpBinPlugin.getDistributorById("binObjects")?.sendOutputData({
      names: [],
      filename: "",
    });

    const processType = selectedTools.get("bin") || "single";
    cfg.ndmspc.process.type = processType;
    const commands = [];
    if (processType === "single") {
      cfg.ndmspc.process.ranges = null;
      if (NdmscpBinPlugin.state.useCache && (await urlExists(f))) {
        console.log(`Url ${f} exists`);
        await getFileObjectNames(f, NdmscpBinPlugin, "binObjects");
        return;
        // console.log(`Url ${f} exists`);
        // } else if (await urlExists(fBase)) {

        //   await getNdmspcFileProjection(fBase, fBaseObject);
        //   console.log(`Url ${f} exists`);
      } else {
        // Execute job

        console.log(cfg);
        const encodedData = btoa(JSON.stringify(cfg));
        const cmd = `${cmd_base} "base64|${encodedData}|ndmspc.json"`;
        commands.push(cmd);
      }
    } else if (selectedTools.get("bin") === "all") {
      console.log("Running 'all' ...");
      cfg.ndmspc.process.ranges = null;
      let min = [1, 1, 1];
      let max = [1, 1, 1];
      const step = [10, 10, 10];
      console.log(histogram._typename);
      if (histogram._typename.startsWith("TH1")) {
        for (let ix = 1; ix <= histogram.fXaxis.fNbins; ix += step[0]) {
          // TODO set multiple jobs vi range
          min[0] = ix;
          max[0] = ix + step[0] - 1;
          if (max > histogram.fXaxis.fNbins) max[0] = histogram.fXaxis.fNbins;
          cfg.ndmspc.process.ranges = new Array([min[0], max[0]]);

          const encodedData = btoa(JSON.stringify(cfg));
          const cmd = `${cmd_base} "base64|${encodedData}|ndmspc.json"`;
          commands.push(cmd);
        }
      } else if (histogram._typename.startsWith("TH2")) {
        for (let iy = 1; iy <= histogram.fYaxis.fNbins; iy += step[1]) {
          min[1] = iy;
          max[1] = iy + step[1] - 1;
          if (max > histogram.fYaxis.fNbins) max[1] = histogram.fYaxis.fNbins;
          for (let ix = 1; ix <= histogram.fXaxis.fNbins; ix += step[0]) {
            min[0] = ix;
            max[0] = ix + step[0] - 1;
            if (max > histogram.fXaxis.fNbins) max[0] = histogram.fXaxis.fNbins;
            cfg.ndmspc.process.ranges = new Array(
              [min[0], max[0]],
              [min[1], max[1]]
            );
            const encodedData = btoa(JSON.stringify(cfg));
            const cmd = `${cmd_base} "base64|${encodedData}|ndmspc.json"`;
            commands.push(cmd);
          }
        }
      } else if (histogram._typename.startsWith("TH3")) {
        for (let iz = 1; iz <= histogram.fZaxis.fNbins; iz++) {
          min[2] = iz;
          max[2] = iz + step[2] - 1;
          if (max > histogram.fZaxis.fNbins) max[1] = histogram.fZaxis.fNbins;
          for (let iy = 1; iy <= histogram.fYaxis.fNbins; iy += step[1]) {
            min[1] = iy;
            max[1] = iy + step[1] - 1;
            if (max > histogram.fYaxis.fNbins) max[1] = histogram.fYaxis.fNbins;
            for (let ix = 1; ix <= histogram.fXaxis.fNbins; ix += step[0]) {
              min[0] = ix;
              max[0] = ix + step[0] - 1;
              if (max > histogram.fXaxis.fNbins)
                max[0] = histogram.fXaxis.fNbins;
              cfg.ndmspc.process.ranges = new Array(
                [min[0], max[0]],
                [min[1], max[1]],
                [min[2], max[2]]
              );
              const encodedData = btoa(JSON.stringify(cfg));
              const cmd = `${cmd_base} "base64|${encodedData}|ndmspc.json"`;
              commands.push(cmd);
            }
          }
        }
      } else {
        console.log("Unsuported histogram ", histogram);
        return;
      }
    }

    // console.log(cfg.ndmspc.process);

    if (clusterInfo?.executorUrl) {
      console.log(clusterInfo?.executorUrl);

      await submitSalsaJob(
        clusterInfo?.executorUrl,
        commands,
        clusterInfo.submitUrl,
        cfg
      )
        .then((res) => {
          console.log(res);
          NdmscpBinPlugin.getDistributorById("job")?.sendOutputData({
            res,
            extra,
          });
        })
        .catch((err) => {
          console.error(err);
        });
    } else {
      console.log(clusterInfo);
      console.log("No executor");
      NdmscpBinPlugin.getDistributorById("job")?.sendOutputData({
        error: "No Executor",
        extra: clusterInfo,
      });
    }
  },
  //   onBinDblClick: (data) => {
  //     // console.log("NDMSPC NdmscpBinPlugin : onBinDblClick()", data);
  //   },
  //   onViewChange: (data) => {
  //     // console.log("NDMSPC NdmscpBinPlugin : onViewChange()", data);
  //   },
  //   onExecuteCommand: (data) => {
  //     // console.log("NDMSPC NdmscpBinPlugin : onExecuteCommand()", data);
  //   },
  //   onProgress: (data) => {
  //     // console.log("NDMSPC NdmscpBinPlugin : onProgress()", data);
  //   },
  //   onContextOption: (data) => {
  //     // console.log("NDMSPC NdmscpBinPlugin : onContextOption()", data);
  //   }
};

export default NdmscpBinPlugin;
