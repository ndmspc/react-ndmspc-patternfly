import { AppGlobalScope, Distributor } from "@ndmspc/react-ndmspc-core";

const EmptyPlugin = new AppGlobalScope({
  name: "EmptyPlugin",
  cluster: null,
  isVR: false,
  workspaces: {
    default: [
      {
        type: "ndmvr",
        span: 6,
        rowSpan: 1,
      },
    ],
  },
});

EmptyPlugin.addDistributor(new Distributor("cluster"));
const handleClusterChange = (data) => {
  EmptyPlugin.state.cluster = data;
};
EmptyPlugin.addHandlerFunc("cluster", handleClusterChange, null);
EmptyPlugin.createSubscription("cluster");

EmptyPlugin.userFunctions = {
  //   onBinHover: (data) => {
  //     // console.log("NDMSPC EmptyPlugin : onBinHover()", data);
  //   },
  onBinClick: (data) => {
    // console.log("NDMSPC EmptyPlugin : onBinClick()", data);
  },
  //   onBinDblClick: (data) => {
  //     // console.log("NDMSPC EmptyPlugin : onBinDblClick()", data);
  //   },
  //   onViewChange: (data) => {
  //     // console.log("NDMSPC EmptyPlugin : onViewChange()", data);
  //   },
  //   onExecuteCommand: (data) => {
  //     // console.log("NDMSPC EmptyPlugin : onExecuteCommand()", data);
  //   },
  //   onProgress: (data) => {
  //     // console.log("NDMSPC EmptyPlugin : onProgress()", data);
  //   },
  //   onContextOption: (data) => {
  //     // console.log("NDMSPC EmptyPlugin : onContextOption()", data);
  //   }
};

export default EmptyPlugin;
