import React, { useLayoutEffect, useRef, useState } from 'react';
import { useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';

export const AutoSizeComponent = ({ content = null, padding = 10 }) => {
  const ref = useRef(null);
  const [width, setWidth] = useState(100);
  const [height, setHeight] = useState(200);
  const [ndmspcInternal,] = useNdmSpcRedirect("ndmspcInternal");

  useLayoutEffect(() => {
    if ((!ndmspcInternal) || (ndmspcInternal?.type === "ui" && ndmspcInternal?.action === "resize")) {
      setWidth(ref.current.parentElement.offsetWidth - padding);
      setHeight(ref.current.parentElement.offsetHeight - padding);
    }

  }, [ndmspcInternal]);

  return (
    <div ref={ref} title={`Element: [${width}, ${height}]`}>
      {content ? React.cloneElement(content, { style: { height, width, position: "sticky"} })
        : (<>
          <h2>Element: [{width}, {height}]</h2>
        </>)}
    </div>
  );
}
