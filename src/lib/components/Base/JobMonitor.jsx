import React, { useState, useEffect } from 'react';
import { useNdmSpcLocal, useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';
import { ActionGroup, Button, Form, Label, Modal, ModalVariant, Progress, ProgressMeasureLocation, Spinner, Split, SplitItem } from '@patternfly/react-core';

export const JobMonitor = ({ jobFinishTimeout = 500, onTitleChange = null }) => {
  const [group, setGroup] = useState(null)
  const [mgr, setMgr] = useState(null)
  const [job, setJob] = useState(null)
  const [tasks, setTasks] = useState(null)
  const [data, setData] = useState(null)
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws,] = useNdmSpcLocal("ndmspcZmq2ws");
  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const [ndmspcJobs,] = useNdmSpcRedirect("ndmspcJobs");

  const [ndmspcNode,] = useNdmSpcRedirect("ndmspcNode");

  // const onShowLogs = () => {
  //   const currentJob = ndmspcJobs.jobs.find(x => x.name === job);
  //   const payload = {
  //     mgr: mgr.split(":")[0],
  //     job : currentJob
  //   }
  //   console.log("Going to show logs for ", payload);

  //   ndmspcInternalStore.setData({ type: "logs", action: "open", payload })
  // }


  useEffect(() => {
    ndmspcInternalStore.setData({ ...ndmspcInternal, type: "job", action: "started" });
    if (onTitleChange) onTitleChange("Submiting ...");

  }, []);
  useEffect(() => {
    if (!ndmspcJobs || !mgr || !job) return

    // console.log(ndmspcJobs)
    if (!ndmspcJobs.jobs) return
    const currentJob = ndmspcJobs.jobs.find(x => x.name === job);
    // console.log(currentJob)
    // TODO handle case when cluster is not same
    if (!currentJob) {
      // ndmspcAppStore.setData({ ...ndmspcApp, isJobMonitorOpen: false, jobMonitorMgr: null, jobMonitorJob: null, jobExtra: null });
      return
    }
    if (onTitleChange) onTitleChange("Processing ...");
    setTasks({ ...currentJob, total: currentJob.P + currentJob.R + currentJob.D + currentJob.F });
    if (currentJob !== undefined && currentJob?.time?.finished && currentJob.F == 0) {
      setTimeout((data) => {
        ndmspcAppStore.setData({ ...ndmspcApp, isJobMonitorOpen: false, jobMonitorMgr: null, jobMonitorJob: null, jobExtra: null });
        if (data)
          ndmspcInternalStore.setData({ ...ndmspcInternal, type: "job", action: "redraw", filename: data.filename, canvas: data?.canvas, bin: data?.bin });
        setMgr(null)
        setJob(null)
        setData(null)
        setTasks(null)
      }, jobFinishTimeout, data);
    }
  }, [ndmspcJobs]);


  useEffect(() => {
    console.log("JobMonitor:", mgr, job)
  }, [mgr, job]);

  useEffect(() => {
    if (!ndmspcApp) return

    // console.log(ndmspcApp.isJobMonitorOpen, ndmspcApp?.jobMonitorMgr, ndmspcApp?.jobMonitorJob, ndmspcApp?.jobExtra)
    if (ndmspcApp.jobMonitorMgr && ndmspcApp.jobMonitorJob) {
      setMgr(ndmspcApp.jobMonitorMgr)
      setJob(ndmspcApp.jobMonitorJob)
      setData(ndmspcApp.jobExtra)
      setGroup(ndmspcZmq2ws?.sub.split(":")[0])
    }
  }, [ndmspcApp]);

  return tasks ? (
    <>
      <Split hasGutter>
        <SplitItem>
          <Label color="blue">Site: {group}</Label>
          <Label color="green">Cluster: {mgr}</Label>
          <Label color="purple">Job: {job}</Label>
        </SplitItem>
      </Split>
      <Split hasGutter>
        <SplitItem>
          <Label>File: {data?.filename}</Label>
        </SplitItem>
      </Split>

      <Split hasGutter>
        <SplitItem isFilled>
          <Progress
            aria-label="Cluster Usage"
            // title=""
            title={tasks && ndmspcNode ? `Using ${tasks.R} of ${ndmspcNode.slots} slots` : ""}
            min={0}
            max={ndmspcNode ? ndmspcNode.slots : 0}
            value={tasks ? tasks.R : 0}
            variant="info"
            // size={ProgressSize.sm}
            measureLocation={ProgressMeasureLocation.top}
          />
        </SplitItem>
      </Split>

      <Split hasGutter>
        <SplitItem isFilled>
          <Progress
            aria-label="Job progress"
            // title=""
            title={tasks ? `Tasks: ${tasks.D + tasks.F}/${tasks.total}` : ""}
            min={0}
            max={tasks ? tasks.total : 0}
            value={tasks ? tasks.D + tasks.F : 0}
            variant={tasks ? (tasks.D == tasks.total) ? "success" : (tasks.F > 0) ? "danger" : "default" : "default"}
            // size={ProgressSize.sm}
            measureLocation={ProgressMeasureLocation.top}
          />
        </SplitItem>
      </Split>
      <Form id="main-confiuration">
        <ActionGroup>
          {/* <Button variant="primary" onClick={onShowLogs} isDisabled={job?.rc ? false : false}>Show Logs</Button> */}
          {/* <Button variant="secondary">Kill</Button>
          <Button variant="secondary">Hide</Button> */}
        </ActionGroup>
      </Form>

    </>) : <Spinner />
};

export const JobMonitorModal = () => {
  const [title, setTitle] = useState("Processing ...");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");

  const onTitleChange = (t) => {
    setTitle(t)
  };

  const handleModalToggle = () => {
    const imo = !isModalOpen
    setIsModalOpen(imo);
    ndmspcAppStore.setData({ ...ndmspcApp, isJobMonitorOpen: imo });
  };

  useEffect(() => {
    // console.log(ndmspcApp)
    setIsModalOpen(ndmspcApp?.isJobMonitorOpen);
  }, [ndmspcApp]);

  // position="top" width="80%"
  return (
    <Modal title={title} variant={ModalVariant.medium} isOpen={isModalOpen} onClose={handleModalToggle} >
      <JobMonitor onTitleChange={onTitleChange} />
    </Modal>
  )
}