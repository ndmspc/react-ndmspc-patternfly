import React from 'react';
import { Accordion, AccordionItem, AccordionContent, AccordionToggle, ExpandableSection } from '@patternfly/react-core';
import { JsonEditor as Editor } from "jsoneditor-react";
import "jsoneditor-react/es/editor.min.css";
export const FormProperties = ({ name = "main", content = null, type = "json" }) => {
  const [expanded, setExpanded] = React.useState(true);
  const onToggle = () => {
    setExpanded(!expanded)
  }

  const handleChange = (e,v) => {
    console.log(e,v);
  }

  return content?.properties ?
    (type === "accordion") ?
      <Accordion asDefinitionList isBordered>
        <AccordionItem key={`${name}`}>
          <AccordionToggle onClick={onToggle} isExpanded={expanded} id={`${name}-toggle`}>
            {name !== "main" ? `${content?.description} (${name})` : expanded ? 'Show less' : 'Show more'}
          </AccordionToggle>
          <AccordionContent id={`${name}-expand`} isHidden={!expanded}>
            <FormObject name={name} content={content?.properties} type={type} />
          </AccordionContent>
        </AccordionItem>
      </Accordion>
      : (type === "expandable") ?
        <ExpandableSection toggleText={name !== "main" ? `${content?.description} (${name})` : expanded ? 'Show less' : 'Show more'} onToggle={onToggle} isExpanded={expanded}>
          <FormObject name={name} content={content?.properties} type={type} />
        </ExpandableSection>
        : (type=== "json")? <Editor value={content?.properties} onChange={handleChange} /> :null


    : null
};