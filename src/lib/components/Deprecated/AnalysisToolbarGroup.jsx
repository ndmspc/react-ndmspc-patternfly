import React, { useEffect, useState } from 'react';
import { Button, Icon, ToolbarGroup, ToolbarItem } from '@patternfly/react-core';
import TimesIcon from '@patternfly/react-icons/dist/js/icons/times-icon';
import PlusCircleIcon from '@patternfly/react-icons/dist/esm/icons/plus-circle-icon';
import UploadIcon from '@patternfly/react-icons/dist/esm/icons/upload-icon';
import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';
export const AnalysisToolbarGroup = () => {
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [analysis, setAnalysis] = useState(null);

  const onClickProfileDeselect = () => {
    ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, analysis: "" });
  };

  const onClickProfileExport = () => {
    const fileData = JSON.stringify(ndmspcAnalyses);
    const blob = new Blob([fileData], { type: "text/plain" });
    const url = URL.createObjectURL(blob);
    const link = document.createElement("a");
    const date = new Date();
    link.download = `ndmspc-${date.getFullYear()}${date.getMonth() + 1}${date.getDate()}-${date.getHours()}${date.getMinutes()}${date.getSeconds()}.json`;
    link.href = url;
    link.click();
  };


  useEffect(() => {
    if (!ndmspcAnalyses) return
    if (!ndmspcAnalyses?.analyses && ndmspcAnalyses?.analysis?.length > 0) {
      setAnalysis(ndmspcAnalyses.analyses[ndmspcAnalyses.analysis])
    } else {
      setAnalysis(null)
    }
  }, [ndmspcAnalyses]);

  return (
    <ToolbarGroup variant="icon-button-group">
      <ToolbarItem variant="bulk-select">
        Analysis
      </ToolbarItem>
      <ToolbarItem>
        <Button variant="tertiary" size="sm" aria-label="close" icon={<TimesIcon />} onClick={onClickProfileDeselect} iconPosition="end">{analysis?.name}</Button>
      </ToolbarItem>
      <ToolbarItem>
        <Button variant="link" size="sm" aria-label="add" icon={<PlusCircleIcon />} onClick={onClickProfileDeselect} >Add</Button>
      </ToolbarItem>
      <ToolbarItem>
        <Button variant="link" size="sm" aria-label="export" icon={<UploadIcon />} onClick={onClickProfileExport} >Export</Button>
      </ToolbarItem>

    </ToolbarGroup >);
};