import { MenuToggle, Select, SelectList, SelectOption } from '@patternfly/react-core';
import React, { useEffect, useState } from 'react';
import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';

export const ProjectionSelect = () => {

  const [isProjectionSelectOpen, setIsProjectionSelectOpen] = useState(false);
  const [selectedProjection, setSelectedProjection] = useState('');
  const [analysis, setAnalysis] = useState(null);

  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");

  useEffect(() => {
    // console.log(ndmspcAnalyses)
    if (!ndmspcAnalyses) return
    if (!ndmspcAnalyses?.analyses) return
    if (!ndmspcAnalyses?.analysis) return
    setAnalysis(ndmspcAnalyses.analyses[ndmspcAnalyses.analysis])
  }, [ndmspcAnalyses]);


  useEffect(() => {
    if (!analysis) return
    // console.log(analysis)
    // console.log(Object.keys(analysis?.projections))
    setSelectedProjection(analysis?.projection)

  }, [analysis]);

  const onProjectionSelect = (_event, name) => {
    ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, analyses: { ...ndmspcAnalyses.analyses, [analysis.name]: {...ndmspcAnalyses?.analyses[analysis.name], projection: name }}});
    setIsProjectionSelectOpen(false);
  }

  const onProjectionSelectToggle = isOpen => {
    setIsProjectionSelectOpen(isOpen);
  };

  // return null
  return (
    <>
    {/* {analysis && analysis?.name} */}
    <Select id="run-select" isOpen={isProjectionSelectOpen} selected={selectedProjection} onSelect={onProjectionSelect} onOpenChange={isOpen => setIsProjectionSelectOpen(isOpen)} toggle={toggleRef => <MenuToggle ref={toggleRef} onClick={onProjectionSelectToggle} isExpanded={isProjectionSelectOpen} isDisabled={false}>{selectedProjection}</MenuToggle>} shouldFocusToggleOnSelect>
      <SelectList>
        {analysis?.projections && Object.keys(analysis?.projections).map((option, index) => <SelectOption key={option} id={`select-projection-${option}`} value={option}>{option}</SelectOption>)}
      </SelectList>
    </Select>
    </>

  );
};