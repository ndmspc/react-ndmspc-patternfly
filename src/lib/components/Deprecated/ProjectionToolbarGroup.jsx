import React, { } from 'react';
import { ToolbarGroup, ToolbarItem } from '@patternfly/react-core';
import { ProjectionSelect } from './ProjectionSelect';
export const ProjectionToolbarGroup = () => {


  return (
    <ToolbarGroup variant="icon-button-group">
      <ToolbarItem variant="bulk-select">
        Projection
      </ToolbarItem>
      <ToolbarItem>
        <ProjectionSelect />
      </ToolbarItem>
    </ToolbarGroup>);
};