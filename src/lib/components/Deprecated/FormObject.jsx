import { useEffect, useState } from 'react';
import { FormGroup, Button, TextInput } from '@patternfly/react-core';
import UndoIcon from '@patternfly/react-icons/dist/esm/icons/undo-icon';
import { FormProperties } from './FormProperties';
export const FormObject = ({ content = null, type="accordion" }) => {
  const [values, setValues] = useState({})

  const onChange = (_event, value) => {
    // console.log("onChange", _event, value)
    setValues({ ...values, [_event.target.name]: { ...values[_event.target.name], value } })
  };

  const onReset = (_event, value) => {
    // console.log("onReset", _event, value)
    setValues({ ...values, [value]: { ...values[value], value: values[value].default } })
  };

  useEffect(() => {
    // console.log(content)
    setValues(content)
  }, [content]);

  return content ?
    (
      Object.keys(content).map((item) => {
        if (content[item].type === "string") {
          // console.log("IT IS STRING", item, content[item])
          return (
            <FormGroup key={`plugin-confiuration-fg-${item}`} label={`${content[item].description} (${item})`} isRequired fieldId={`plugin-confiuration-${item}`}
              labelIcon={
                <Button variant="plain" aria-label="edit" onClick={(_event, d = `${item}`) => onReset(_event, d)}>
                  <UndoIcon />
                </Button>
              }
            >
              <TextInput isRequired type="text" key={`plugin-confiuration-${item}`} id={`plugin-confiuration-${item}`} name={item} value={values ? (values[item]?.value || content[item]?.default) : ""} aria-label="text input example" onChange={onChange} />
            </FormGroup>
          )
        }
        else if (content[item].type === "integer" || content[item].type === "float") {
          // console.log("IT IS NUMBER", item, content[item])
          return (
            <FormGroup key={`plugin-confiuration-fg-${item}`} label={`${content[item].description} (${item})`} isRequired fieldId={`plugin-confiuration-${item}`}>
              <TextInput isRequired type="number" key={`plugin-confiuration-${item}`} id={`plugin-confiuration-${item}`} name={item} value={values ? (values[item]?.value || content[item]?.default) : ""} onChange={onChange} />
            </FormGroup>
          )
        } else if (content[item].type === undefined) {
          // console.log("IT IS PROPERTIES", item, content[item])
          if (content[item]?.properties) {
            return <FormProperties key={`properties-${item}`} name={item} content={content[item]} type={type}/>
            // console.log(contentProperties[item].description)
          }
        }
      })
    )
    : null

};