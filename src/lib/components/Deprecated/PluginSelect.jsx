import { MenuToggle, Select, SelectList, SelectOption } from '@patternfly/react-core';
import React, { useEffect, useState } from 'react';
import { useNdmSpcLocal, useNdmSpcPlugins } from '@ndmspc/react-ndmspc-core';
import { PluginOptionSelect } from './PluginOptionSelect';

export const PluginSelect = () => {

  const [isPluginSelectOpen, setIsPluginSelectOpen] = useState(false);
  const [selectedPlugin, setSelectedPlugin] = useState(null);
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const ndmspcPlugins = useNdmSpcPlugins()

  useEffect(() => {
    if (!ndmspcAnalyses) return;

    if (ndmspcAnalyses?.plugin) {
      setSelectedPlugin(ndmspcAnalyses?.plugin);
    }
  }, [ndmspcAnalyses]);


  useEffect(() => {
    if (!ndmspcPlugins) return;
    // console.log(ndmspcPlugins)
    setSelectedPlugin(Object.keys(ndmspcPlugins)[0]);
  }, [ndmspcPlugins]);

  useEffect(() => {
    if (!selectedPlugin) return;
    if (!ndmspcAnalyses) return
    ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, plugin: selectedPlugin });
  }, [selectedPlugin]);


  const onPluginSelect = (_event, name) => {
    setSelectedPlugin(name)
    setIsPluginSelectOpen(false);
  }

  return (
    <>
      <Select id="run-select" isOpen={isPluginSelectOpen} selected={selectedPlugin} onSelect={onPluginSelect} onOpenChange={isOpen => setIsPluginSelectOpen(isOpen)} toggle={toggleRef => <MenuToggle ref={toggleRef} onClick={isOpen => setIsPluginSelectOpen(isOpen)} isExpanded={isPluginSelectOpen} isDisabled={false}>{selectedPlugin}</MenuToggle>} shouldFocusToggleOnSelect>
        <SelectList>
          {ndmspcPlugins && Object.keys(ndmspcPlugins).map((option) => <SelectOption key={`select-plugin-${option}`} id={`select-plugin-${option}`} value={option}>{option}</SelectOption>)}
        </SelectList>
      </Select>

    </>

  );
};