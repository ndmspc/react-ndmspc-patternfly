import React, { useEffect, useState } from 'react';
import { Text, MenuToggle, Select, SelectList, SelectOption, Button, Tooltip, Icon } from '@patternfly/react-core';
import SyncIcon from '@patternfly/react-icons/dist/esm/icons/sync-icon';
import CogIcon from '@patternfly/react-icons/dist/esm/icons/cog-icon';

import { useNdmSpcLocal, useNdmSpcPlugin } from '@ndmspc/react-ndmspc-core';
import { PluginConfigurationModal } from '../Plugin/PluginConfiguration';

export const PluginOptionSelect = ({ name, title, type = "select", icon = null, options, histDim = 1 }) => {

  const [isPluginOptionSelectOpen, setIsPluginOptionSelectOpen] = useState(false);
  const [selectedPluginOption, setSelectedPluginOption] = useState(options[histDim - 1][0]);
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  useEffect(() => {
    if (ndmspcCurrentPlugin)
      ndmspcCurrentPlugin.getDistributorById("selectedPluginTool")?.sendInputData({ name, value: selectedPluginOption });
  }, [selectedPluginOption, ndmspcCurrentPlugin]);

  // const handleSelectedPlugin = (data) => {
  //   console.log("handleSelectedPlugin", data)
  //   if (name === data.name)
  //     setSelectedPluginOption(data.value);
  // }

  // useEffect(() => {
  //   const subscription = ndmspcCurrentPlugin.createSubscription(
  //     "selectedPluginTool",
  //     handleSelectedPlugin,
  //     null
  //   );
  //   return () => subscription?.unsubscribe();
  // }, [ndmspcCurrentPlugin]);



  useEffect(() => {
    setSelectedPluginOption(options[histDim - 1][0])
  }, [options]);

  const onPluginOptionSelect = (_event, value) => {
    setSelectedPluginOption(value)
    ndmspcCurrentPlugin.getDistributorById("selectedPluginTool")?.sendInputData({ name, value });
    setIsPluginOptionSelectOpen(false)
  }
  const onButtonClick = () => {
    // console.log("Cliked ", name, title, type)
  }


  const onRefreshClick = () => {
    // console.log("Cliked ", name, title, type)
    // if (!ndmspcAnalyses) return
    // ndmspcCurrentPlugin.userFunctions?.onScanHistogram({ analysis: ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis] })
  }

  const onConfigurationClick = () => {
    // console.log("onConfigurationClick ", name, title, type)
    if (!ndmspcApp) return
    ndmspcAppStore.setData({...ndmspcApp, isPluginConfigurationOpen: true })
  }




  const renderOption = () => {
    // console.log(name, title, type)
    if (type === "select") {
      return (
        <>
          <Select id={`${name}-select`} isOpen={isPluginOptionSelectOpen} selected={selectedPluginOption} onSelect={onPluginOptionSelect} onOpenChange={isOpen => setIsPluginOptionSelectOpen(isOpen)} toggle={toggleRef => <MenuToggle ref={toggleRef} onClick={isOpen => setIsPluginOptionSelectOpen(isOpen)} isExpanded={isPluginOptionSelectOpen} isDisabled={false}>{selectedPluginOption}</MenuToggle>} shouldFocusToggleOnSelect>
            <SelectList>
              {options[histDim - 1] && options[histDim - 1].map((option, index) => <SelectOption key={option} id={`${name}-select-plugin`} value={option}>{option}</SelectOption>)}
            </SelectList>
          </Select>
        </>)
    }
    else if (type === "button") {
      return (
        <>
          <Button onClick={onButtonClick}>{title}</Button>
        </>)
    }

    else if (type === "icon" && icon == "sync") {
      return (<Button variant="tertiary" size="sm" aria-label="close" icon={icon === "sync" ? <SyncIcon /> : null} onClick={onRefreshClick} iconPosition="end">{title}</Button>)
    }

    else if (type === "configuration") {
      return (
        <>
          
          <Button variant="tertiary" size="sm" aria-label="close" icon={<CogIcon />} onClick={onConfigurationClick} iconPosition="end">{title}</Button>
        </>)
    }



    return null
  }

  return options && options[histDim - 1] && ndmspcCurrentPlugin ?
    <>
      {renderOption()}
    </>
    : null
};