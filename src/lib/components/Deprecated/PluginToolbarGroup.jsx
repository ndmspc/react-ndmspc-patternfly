import React, { } from 'react';
import { ToolbarGroup, ToolbarItem } from '@patternfly/react-core';
import { PluginSelect } from './PluginSelect';
export const PluginToolbarGroup = () => {


  return (
    <ToolbarGroup variant="icon-button-group">
      <ToolbarItem variant="bulk-select">
        Plugin
      </ToolbarItem>
      <ToolbarItem>
        <PluginSelect />
      </ToolbarItem>
    </ToolbarGroup>);
};