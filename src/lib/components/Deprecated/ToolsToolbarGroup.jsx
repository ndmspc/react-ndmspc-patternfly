import React, { useEffect, useState } from 'react';
import { ToolbarGroup, ToolbarItem, Tooltip } from '@patternfly/react-core';
import { PluginSelect } from './PluginSelect';
import { PluginOptionSelect } from './PluginOptionSelect';
import { useNdmSpcLocal, useNdmSpcPlugin } from '@ndmspc/react-ndmspc-core';
export const ToolsToolbarGroup = () => {

  const [pluginTools, setPluginTools] = useState(null);

  // const [isPluginSelectOpen, setIsPluginSelectOpen] = useState(false);
  // const [selectedPlugin, setSelectedPlugin] = useState('');
  const [histogramDimension, setHistogramDimension] = useState(1);
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()
  // const ndmspcPlugins = useNdmSpcPlugins()

  useEffect(() => {
    if (!ndmspcAnalyses) return
    if (!ndmspcAnalyses?.analyses) return
    if (!ndmspcAnalyses?.analysis) return
    const projection = ndmspcAnalyses?.analyses[ndmspcAnalyses?.analysis]?.projection
    if (projection)
      setHistogramDimension(projection.split("_").length)

  }, [ndmspcAnalyses]);

  useEffect(() => {
    setPluginTools(ndmspcCurrentPlugin?.state?.tools)
  }, [ndmspcCurrentPlugin]);




  return (
    <ToolbarGroup variant="icon-button-group">
      <ToolbarItem variant="bulk-select">Tools</ToolbarItem>
      {pluginTools ? Object.keys(pluginTools).map((tool) => (
        <ToolbarItem key={`${tool}-item`} variant="bulk-select">
          {/* <Tooltip key={`${tool}-tooltip`} position={"auto"} exitDelay={0} enableFlip content={<div key={`${tool}-tooltip-content`}>{pluginTools[tool]?.tooltip || tool}</div>}> */}
            <PluginOptionSelect key={`${tool}-tool`} name={tool} title={pluginTools[tool].title} type={pluginTools[tool].type} icon={pluginTools[tool]?.icon} options={pluginTools[tool].options} histDim={histogramDimension} />
          {/* </Tooltip> */}
        </ToolbarItem>
      ))
        :
        null}

    </ToolbarGroup>);
};