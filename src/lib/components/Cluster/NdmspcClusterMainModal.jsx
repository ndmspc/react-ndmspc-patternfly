import React, { useState, useEffect, useContext } from 'react'
import { Button, InputGroup, Modal, TextInput, InputGroupItem, Text, FormSelect, FormSelectOption, FormSelectOptionGroup, HelperText, HelperTextItem } from '@patternfly/react-core'
import { useNdmSpcLocal, useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';
import { JobTable } from './JobTable';
import { ClustersTabSelect } from './ClustersTabSelect';

const defaultUrls = [{
  groupLabel: 'Public',
  disabled: false,
  options: [{
    value: 'wss://one.ndmspc.io/zmq2ws',
    label: 'NDMSPC',
    disabled: false
  }, {
    value: 'wss://ndmspc.cern.ch/zmq2ws',
    label: 'CERN',
    disabled: false
  }]
}, {
  groupLabel: 'Local',
  disabled: false,
  options: [{
    value: 'ws://ndmspc.127.0.0.1.nip.io:8008/zmq2ws',
    label: 'ws://ndmspc.127.0.0.1.nip.io:8008/zmq2ws',
    disabled: false
  }, {
    value: 'ws://zmq2ws.127.0.0.1.nip.io:8008',
    label: 'ws://zmq2ws.127.0.0.1.nip.io:8008',
    disabled: false
  }]

}];

export const NdmspcClusterMain = ({ name = "zmq2ws", showTable = false }) => {

  const [connected, setConnected] = useState(false);
  const [loading, setLoading] = useState(false);
  const [app, setApp] = useState(null);
  const [subscribed, setSubscribed] = useState(false);
  const [urls, setUrls] = useState(defaultUrls);
  const [url, setUrl] = useState("wss://one.ndmspc.io/zmq2ws");
  const [prevUrl, setPrevUrl] = useState(null);
  const [ndmspcApp,] = useNdmSpcRedirect("ndmspcApp");
  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const [ndmspcZmq2ws, ndmspcZmq2wsStore] = useNdmSpcLocal("ndmspcZmq2ws");

  function handleWebSocketConnect() {

    setLoading(true)
    if (connected) {
      ndmspcInternalStore.setData({ type: "ws", action: "disconnect" });
      // console.log("NdmspcClusterMain", connected)
      setPrevUrl(null)
    } else {
      if (url.length > 0) ndmspcInternalStore.setData({ type: "ws", action: "connect", url });
    }
  }

  useEffect(() => {
    if (!ndmspcApp) return;

    if (ndmspcApp?.urls) {
      console.log(ndmspcApp?.urls)
      setUrls(ndmspcApp?.urls)
    }
  }, [ndmspcApp]);


  useEffect(() => {
    // console.log("NdmspcClusterMain", ndmspcInternal)
    if (ndmspcInternal?.type === "zmq2ws") {
      if (ndmspcInternal?.action === "subscribed") {
        setSubscribed(true)
        ndmspcZmq2wsStore.setData({ ...ndmspcZmq2ws, subscribed: true });
      } else if (ndmspcInternal?.action === "unsubscribed") {
        setSubscribed(false)
        ndmspcZmq2wsStore.setData({ ...ndmspcZmq2ws, subscribed: false });
      }
    } else if (ndmspcInternal?.type === "api" && ndmspcInternal?.action === "state" && ndmspcInternal?.url?.length > 0) {
      console.log("NdmspcClusterMain::api::state", ndmspcInternal)
      setPrevUrl(ndmspcInternal?.url)
    }
  }, [ndmspcInternal]);

  useEffect(() => {
    if (!ndmspcZmq2ws) return

    // console.log("NdmspcClusterMain",ndmspcZmq2ws)
    setConnected(ndmspcZmq2ws?.connected);
    setLoading(ndmspcZmq2ws?.loading);
    // setUrl(ndmspcZmq2ws?.url || defaultUrl);
    if (ndmspcZmq2ws?.connected) {
      setApp(ndmspcZmq2ws?.app?.payload || null);
      setSubscribed(ndmspcZmq2ws?.subscribed || false);
    } else {
      setApp(null)
      setSubscribed(false)
    }
  }, [ndmspcZmq2ws]);

  return (
    <>
      <InputGroup>
        <InputGroupItem isFill >
          <FormSelect value={url} onChange={(_event, value) => setUrl(value)} isDisabled={connected} aria-label="FormSelect Input">
            {urls.map((group, index) => <FormSelectOptionGroup isDisabled={group.disabled} key={index} label={group.groupLabel}>
              {group.options.map((option, i) => <FormSelectOption isDisabled={option.disabled} key={i} value={option.value} label={option.label} />)}
            </FormSelectOptionGroup>)}
          </FormSelect>
        </InputGroupItem>
        <InputGroupItem>
          <Button id='fileOpen' variant='control' onClick={handleWebSocketConnect}>
            {connected ? "Disconnect" : loading ? "Connecting ..." : "Connect"}
          </Button>
        </InputGroupItem>
      </InputGroup>
      <HelperText>
        {loading ? <HelperTextItem variant="warning" hasIcon>
          Connecting ...
        </HelperTextItem> :
          connected ?
            <HelperTextItem variant="success" hasIcon>
              Connected to {url}
            </HelperTextItem> :
            prevUrl ?
              <HelperTextItem variant="error" hasIcon>
                Disconnected from {prevUrl}.
              </HelperTextItem> :
              <HelperTextItem variant="error" hasIcon>
                Connect to cluster first.
              </HelperTextItem>
        }
      </HelperText>
      {app && <h1>{app.name} {app.version} </h1>}

      {connected && <ClustersTabSelect connected={connected} />}
      {showTable && connected && subscribed && <JobTable />}
    </>
  )
}

export const NdmspcClusterMainModal = ({ title = "Ndmspc Cluster" }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const handleModalToggle = () => {
    if (ndmspcApp && ndmspcApp?.isZmq2wsOpen) {
      ndmspcAppStore.setData({ ...ndmspcApp, isZmq2wsOpen: !ndmspcApp.isZmq2wsOpen });
    } else {
      ndmspcAppStore.setData({ ...ndmspcApp, isZmq2wsOpen: true });
    }
  };

  useEffect(() => {
    // if (!ndmspcApp) return
    if (ndmspcApp && ndmspcApp?.isZmq2wsOpen) {
      setIsModalOpen(ndmspcApp?.isZmq2wsOpen);
    } else {
      setIsModalOpen(false);
    }
  }, [ndmspcApp]);

  return (
    <Modal position="top" width="80%" title={title} isOpen={isModalOpen} onClose={handleModalToggle} >
      <NdmspcClusterMain showTable={true} />
    </Modal>
  )
}


