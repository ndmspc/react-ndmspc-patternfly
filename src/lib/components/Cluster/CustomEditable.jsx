import { Icon, Label } from "@patternfly/react-core";
import React from "react";

import { PencilAltIcon, TimesIcon } from "@patternfly/react-icons";

export const useOutsideClick = (ref, onOutsideClick) => {
  React.useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        onOutsideClick(event);
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
};

export const CustomEditable = ({
  defaultValue,
  placeholder,
  onSubmit,
  onClose,
}) => {
  const [value, setValue] = React.useState(defaultValue);
  const [isEditable, setEditable] = React.useState(false);
  const formRef = React.useRef(null);

  const handleOutsideClick = (event) => {
    setEditable(false);
    setValue(defaultValue);
  };

  React.useEffect(() => {
    setValue(defaultValue);
  }, [defaultValue]);

  useOutsideClick(formRef, handleOutsideClick);

  return isEditable ? (
    <form
      ref={formRef}
      onSubmit={(event) => {
        event.preventDefault();
        onSubmit(value);
        setEditable(false);
      }}
    >
      <input
        placeholder={placeholder}
        name="executor"
        style={{
          minWidth: "25ch",
          fontSize: "0.9rem",
          width: value.length + "ex",
        }}
        onChange={(event) => setValue(event.target.value)}
        value={value}
      />
      <button style={{ display: "none" }} type="submit"></button>
      <button
        onClick={(event) => {
          setEditable(false);
          onClose();
          setValue(defaultValue);
        }}
        style={{
          border: "1px solid gray",
          borderStartEndRadius: "4px",
          borderEndEndRadius: "4px",
          backgroundColor: "gray",
          paddingTop: "1px",
        }}
      >
        <Icon>
          <TimesIcon style={{ paddingTop: "2px" }} color="white" />
        </Icon>
      </button>
    </form>
  ) : (
    <Label
      style={{ marginLeft: "3px" }}
      color={"gray"}
      closeBtn={EditButton({
        onEdit: () => {
          setEditable(true);
        },
      })}
      onClose={() => { }}
    >
      {defaultValue}
    </Label>
  );
};

const EditButton = ({ onEdit }) => {
  return (
    <Icon
      onClick={onEdit}
      style={{ marginLeft: "8px" }}
      size="md"
      iconSize="md"
      color="gray"
    >
      <PencilAltIcon />
    </Icon>
  );
};
