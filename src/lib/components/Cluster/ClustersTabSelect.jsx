/* eslint-disable react/prop-types */
import React, { useState, useMemo, useContext, useEffect } from "react";
import { Tabs, Tab, Spinner, TabTitleText, TabAction, Popover } from "@patternfly/react-core";
import HelpIcon from '@patternfly/react-icons/dist/esm/icons/help-icon';
import { NdmSpcContext, useNdmSpcLocal, useNdmSpcRedirect, getKeyValueFromZmq2wsServicesArray } from "@ndmspc/react-ndmspc-core";

export const ClustersTabSelect = ({ name = "zmq2ws", zmqClear = null, connected = false, zmqSource = "salsa" }) => {

  const sb = useContext(NdmSpcContext)[name];
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws, ndmspcZmq2wsStore] = useNdmSpcLocal("ndmspcZmq2ws");
  const [ndmspcClusterSubscribe, ndmspcClusterSubscribeStore] = useNdmSpcRedirect("ndmspcClusterSubscribe");

  const [activeTabKeySite, setActiveTabKeySite] = useState(null)
  const [activeTabKeyCluster, setActiveTabKeyCluster] = useState(null)
  const [activeTabKeySource, setActiveTabKeySource] = useState(zmqSource)
  // const [isSub, setIsSub] = useState(false)

  const servicesTree = useMemo(() => {
    // console.log(ndmspcApp);
    if (ndmspcZmq2ws?.app?.payload?.services) {
      let tree = getKeyValueFromZmq2wsServicesArray(ndmspcZmq2ws?.app?.payload?.services);
      // console.log(tree);

      return tree;
    }
  }, [ndmspcZmq2ws]);

  useEffect(() => {
    // console.log(activeTabKeySite, activeTabKeyCluster, ndmspcApp?.sub)

    if (activeTabKeySite && activeTabKeyCluster) {
      ndmspcClusterSubscribeStore.setData({ action: "subscribe", name: activeTabKeySite, sub: activeTabKeyCluster, src: activeTabKeySource });
    } else {
      if (ndmspcZmq2ws) {
        if (ndmspcZmq2ws?.sub && ndmspcZmq2ws?.sub.length > 0)
          ndmspcClusterSubscribeStore.setData({ action: "unsubscribe", name: ndmspcZmq2ws?.sub.split('|')[0], sub: ndmspcZmq2ws?.sub.split('|')[1], src: ndmspcZmq2ws?.sub.split('|')[2] });
      }
    }
  }, [activeTabKeyCluster]);


  useEffect(() => {
    if (!ndmspcZmq2ws) return

    if (!activeTabKeySite) {
      if (ndmspcZmq2ws?.sub && ndmspcZmq2ws?.sub.length > 0) {
        setActiveTabKeySite(ndmspcZmq2ws?.sub.split('|')[0])
        setActiveTabKeyCluster(ndmspcZmq2ws?.sub.split('|')[1])
        setActiveTabKeySource(ndmspcZmq2ws?.sub.split('|')[2])
      }
    }

  }, [ndmspcZmq2ws]);


  const handleTabClickSites = (event, tabIndex) => {
    // console.log(activeTabKeySite, tabIndex)
    if (activeTabKeySite !== tabIndex) {
      setActiveTabKeySite(tabIndex);
    } else {
      setActiveTabKeySite(null);
    }
    setActiveTabKeyCluster(null);
    setActiveTabKeySource(zmqSource)
  };

  const handleTabClickClusters = (event, tabIndex) => {
    // console.log(activeTabKeyCluster, tabIndex)
    if (activeTabKeyCluster !== tabIndex) {
      setActiveTabKeyCluster(tabIndex);
    } else {
      setActiveTabKeyCluster(null);
      setActiveTabKeySource(zmqSource)
    }
  };

  return (
    <>
      {servicesTree ? (
        <Tabs
          activeKey={activeTabKeySite}
          onSelect={handleTabClickSites}
          key={`site-tabs`}
        >
          {Object.keys(servicesTree).map((name, idx) => {
            return (
              <Tab
                key={`${name}-${idx}-tab`}
                eventKey={name}
                title={<TabTitleText>{name}</TabTitleText>}
              >
                <Tabs
                  activeKey={activeTabKeyCluster}
                  isSecondary={false}
                  onSelect={handleTabClickClusters}
                  role="region"
                  key={`${name}-${idx}-tabs`}
                >
                  {name &&
                    servicesTree[name].subs.map((sub, idy) => {
                      const uuid = sub.split(':')[0]
                      const alias = sub.split(':')[1]
                      const tabVal = alias.length > 0 ? alias : uuid
                      return (
                        // <Popover
                        //   key={`${name}-${idx}-${idy}-popover`}
                        //   position={"bottom"}
                        //   triggerAction="hover"
                        //   headerContent={<div key={`${name}-${idx}-${idy}-popover-header`}>{`${name}::${tabVal}`}</div>}
                        //   bodyContent={<div key={`${name}-${idx}-${idy}-popover-body`}>{`id: ${uuid}`}</div>}
                        // >
                          <Tab
                            key={`${name}-${idx}-${idy}-tab`}
                            eventKey={sub}
                            title={<TabTitleText>{tabVal}</TabTitleText>}
                          />
                        // </Popover>
                      );
                    })}
                </Tabs>
              </Tab>
            );
          })}
        </Tabs>
      ) : (
        <Spinner size="lg" aria-label="Loading" />
      )}
    </>
  );
};
