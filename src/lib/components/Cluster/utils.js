import { ProgressVariant } from "@patternfly/react-core";
import humanizeDuration from "humanize-duration";
import { clearFinished, killAll, killJob } from "./jobCommands";
//import axios from "axios";

/**
 * specifies structure of Job row in table
 * @param job data
 * @param submitUrl
 * @param executorUrl
 * @returns structure
 */

const shortEnglishHumanizer = humanizeDuration.humanizer({
  language: "shortEn",
  languages: {
    shortEn: {
      y: () => "y",
      mo: () => "mo",
      w: () => "w",
      d: () => "d",
      h: () => "h",
      m: () => "m",
      s: () => "s",
      ms: () => "ms",
    },
  },
});

export const mapJobs = (
  job,
  mgr,
  submitUrl,
  executorUrl,
  ndmspcInternalStore
) => {
  let total = job.P + job.R + job.D + job.F;
  let isProgressDone = job.D === total;
  return [
    { colName: "Name", value: job.name, started: job.time.started * 1000 },
    { colName: "Total", value: total },
    { colName: "P", value: job.P },
    { colName: "R", value: job.R },
    { colName: "D", value: job.D },
    { colName: "F", value: job.F },

    {
      colName: "Duration",
      value: shortEnglishHumanizer(
        (job.time.finished ? job.time.finished * 1000 : Date.now()) -
          job.time.started * 1000,
        {
          shortEn: {
            d: () => "d",
            h: () => "h",
            m: () => "m",
            s: () => "s",
            ms: () => "ms",
          },
          round: true,
          largest: 2, //displays max 2 units
        }
      ),
    },
    {
      colName: "Progress",
      value: {
        val: parseInt(((job.D + job.F) / total) * 100),
        variant: progressVariant(job, total),
        progress: true,
      },
    },
    {
      colName: "Action",
      value: {
        label: !isProgressDone ? "Kill job" : "Clear job",
        variant: !isProgressDone ? "danger" : "warning",
        onClick: () => {
          killJob(submitUrl, executorUrl, job.name);
        },
        action: true,
      },
    },
    {
      colName: "Logs",
      value: {
        label: "Show logs",
        variant: "info",
        onClick: () => {
          
          const payload = {
            mgr: mgr.split(":")[0],
            job
          }
          // console.log("Going to show logs for ", payload);
          ndmspcInternalStore.setData({ type: "logs", action: "open", payload})
        },
        action: true,
      },
    },
  ];
};

/**
 * specifies structure of Total row in table
 * @param  total data
 * @param  node provides number of slots and submitUrl
 * @param  count number of jobs
 * @param  executorUrl
 * @returns structure
 */
export const totalTransform = (total, node, count, executorUrl) => {
  return [
    { colName: "Name", value: "Total (" + count + " jobs)" },
    { colName: "Total", value: total.P + total.R + total.D + total.F || 0 },
    { colName: "P", value: total.P || 0 },
    { colName: "R", value: total.R || 0 },
    { colName: "D", value: total.D || 0 },
    { colName: "F", value: total.F || 0 },
    { colName: "Duration", value: "" },
    {
      colName: "Progress",
      value: "",
      // value: {
      //   progress: true,
      //   val: parseInt((total.R / node.slots) * 100),
      // },
    },
    {
      colName: "Action",
      value: "",
      // value:
      //   count != 0
      //     ? {
      //         label: "KILL ALL",
      //         variant: "danger",
      //         onClick: () => {
      //           killAll(node.submitUrl, executorUrl);
      //         },
      //         action: true,
      //       }
      //     : "---",
    },
    {
      colName: "Logs",
      value: "",
    },
  ];
};

/**
 * specifies structure of Average row in table
 * @param  total data
 * @param  count number of jobs
 * @returns structure
 */
export const averageTransform = (
  total,
  count,
  finished,
  submitUrl,
  executorUrl
) => {
  return [
    { colName: "Name", value: "Average" },
    {
      colName: "Total",
      value: getAvgNum((total.P + total.R + total.D + total.F) / count),
    },
    { colName: "P", value: getAvgNum(total.P / count, 1) },
    { colName: "R", value: getAvgNum(total.R / count, 1) },
    { colName: "D", value: getAvgNum(total.D / count, 1) },
    { colName: "F", value: getAvgNum(total.F / count, 1) },
    { colName: "Duration", value: "---" },
    { colName: "Progress", value: "---" },
    {
      colName: "Action",
      value: finished
        ? {
            label: "Clear Finished",
            variant: "warning",
            onClick: () => {
              clearFinished(submitUrl, executorUrl);
            },
            action: true,
          }
        : "---",
    },
    {
      colName: "Logs",
      value: "",
    },
  ];
};

const getAvgNum = (num, fixedNum) => {
  return !num ? 0 : num % 1 != 0 ? num.toFixed(fixedNum) : num;
};

const progressVariant = (job, total) => {
  if (job.D + job.F < total) {
    return null;
  }
  return job.D === total
    ? ProgressVariant.success
    : job.F === total
    ? ProgressVariant.danger
    : ProgressVariant.warning;
};
