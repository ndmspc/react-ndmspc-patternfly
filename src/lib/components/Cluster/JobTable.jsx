/* eslint-disable react/prop-types */
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Flex,
  FlexItem,
  Label,
  Level,
  LevelItem,
  OverflowMenu,
  OverflowMenuContent,
  OverflowMenuGroup,
  OverflowMenuItem,
  Progress,
  ProgressMeasureLocation,
  ProgressSize,
  Split,
  SplitItem,
  Text,
  TextContent,
  TextVariants,
} from "@patternfly/react-core";
import {
  Table /* data-codemods */,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@patternfly/react-table";
import React, { useMemo, useEffect, useState } from "react";
import { averageTransform, mapJobs, totalTransform } from "./utils";
import { transformSubmitUrl } from "@ndmspc/react-ndmspc-core"
import { clearFinished, killAll } from "./jobCommands";
import { useNdmSpcLocal, useNdmSpcRedirect } from "@ndmspc/react-ndmspc-core";

/**
 * JobTable displays jobs and there lifecicle
 * @param zmq zmq service object
 * @param executorUrl url for executor
 */
export const JobTable = ({ }) => {
  const [executorUrl, setExecutorUrl] = useState('')
  const [ndmspcNode,] = useNdmSpcRedirect("ndmspcNode");
  const [ndmspcJobs,] = useNdmSpcRedirect("ndmspcJobs");
  const [ndmspcZmq2ws,] = useNdmSpcLocal("ndmspcZmq2ws");
  const [,ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  

  const jobs = useMemo(() => {
    if (ndmspcJobs == null || ndmspcNode == null) return null;
    let jobs = ndmspcJobs?.jobs;
    let node = ndmspcNode;
    if (!jobs) return;
    // console.log(ndmspcJobs)
    let finished = jobs.some((job) => job.D === job.P + job.R + job.D + job.F);
    if (jobs && jobs.length > 0) {
      let tempJobs = jobs.map((job) => {
        return mapJobs(job, ndmspcJobs?.mgr ,node?.submitUrl, executorUrl, ndmspcInternalStore);
      });

      let total = jobs.reduce(
        (acc, item) => ({
          P: (acc.P || 0) + item.P,
          R: (acc.R || 0) + item.R,
          D: (acc.D || 0) + item.D,
          F: (acc.F || 0) + item.F,
        }),
        {}
      );

      let tempTotal = totalTransform(total, node, jobs.length, executorUrl);
      let tempAverage = averageTransform(
        total,
        jobs.length,
        finished,
        node.submitUrl,
        executorUrl
      );

      // console.log(tempJobs)

      tempJobs.sort((a, b) => a[0].started - b[0].started);
      tempJobs.reverse(); // change order of array to set latest job on top
      // tempJobs.unshift(tempAverage);
      tempJobs.unshift(tempTotal);

      return tempJobs;
    }
    return null;
  }, [ndmspcNode, ndmspcJobs, executorUrl]);

  useEffect(() => {
    if (!ndmspcZmq2ws) return
    setExecutorUrl(ndmspcZmq2ws?.executorUrl)
  }, [ndmspcZmq2ws]);

  return (
    <>
      <FlexItem>
        <Card isPlain>
          <CardTitle>Cluster information</CardTitle>
        </Card>
        {ndmspcNode && (
          <Split hasGutter>


            <SplitItem>
              <Label>Number of workers: {ndmspcNode.slots}</Label>
            </SplitItem>
            <SplitItem>
              <Label>Submit url: {transformSubmitUrl(ndmspcNode.submitUrl)}</Label>
            </SplitItem>
            <SplitItem isFilled>

              <Progress
                aria-label="Job progress"
                title="Cluster usage: "
                value={jobs ? jobs[0][3].value / ndmspcNode.slots * 100 : 0}
                variant="default"
                size={ProgressSize.sm}
              // measureLocation={ProgressMeasureLocation.none}
              />
            </SplitItem>
            <SplitItem>
              <Button
                size="sm"
                variant="warning"
                onClick={() => { clearFinished(transformSubmitUrl(ndmspcNode.submitUrl), executorUrl) }}
              >
                Clear finished
              </Button>
            </SplitItem>

            <SplitItem>
              <Button
                size="sm"
                variant="danger"
                onClick={() => { killAll(transformSubmitUrl(ndmspcNode.submitUrl), executorUrl) }}
              >
                KILL ALL
              </Button>
            </SplitItem>



          </Split>
        )}

        {jobs && jobs.length > 0 ? (
          <>



            <Table aria-label="Simple table" variant="compact">
              <Thead>
                <Tr>
                  {jobs[0].map((column, columnIndex) => (
                    <Th key={columnIndex}>{column.colName}</Th>
                  ))}
                </Tr>
              </Thead>
              <Tbody>
                {jobs.map((element, i) => (
                  <Tr
                    key={i}
                    // className={i < 1 && "table-total-row"} // setting different styling for average and total row
                    style={i < 1 ? { backgroundColor: 'var(--pf-v5-global--BackgroundColor--200)' } : {}}
                  >
                    {element.map((column, index) =>
                      column.value.progress != null ? ( // show progress bar
                        <Td key={index} dataLabel={column.colName}>
                          <Progress
                            aria-label="Job progress"
                            value={column.value.val}
                            variant={column.value.variant}
                            measureLocation={ProgressMeasureLocation.none}
                          />
                        </Td>
                      ) : column.value.action ? ( //show Actions
                        <Td key={index}>
                          <Button
                            variant={column.value.variant}
                            onClick={column.value.onClick}
                            size="sm"
                          >
                            {column.value.label}
                          </Button>
                        </Td>
                      ) : (
                        // show values
                        <Td key={index} dataLabel={column.colName}>
                          {column.value}
                        </Td>
                      )
                    )}
                  </Tr>
                ))}
              </Tbody>

            </Table>
          </>
        ) : (
          <Text>NO JOBS</Text>
        )}
      </FlexItem >
    </>
  );
};
