import axios from "axios";

// curl EXAMPLE:
/* curl -X POST -H 'Content-Type: application/json' -d '{"type":"salsa",
"subtype":"feeder",
"salsa":{"host":"tcp://eos0.ndmspc.io:41000","job_type":"command"},
"command":"JOB_DEL_ALL",
"args":"1"}' http://executor.127.0.0.1.nip.io:8008*/

/**
 * remove all jobs from job manager
 * @param submitUrl
 * @param executorUrl
 */
export const killAll = (submitUrl, executorUrl) => {
  axios
    .post(
      executorUrl,
      {
        type: "salsa",
        subtype: "feeder",
        salsa: { host: submitUrl, job_type: "command" },
        command: "JOB_DEL_ALL",
        args: "",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((data) => console.log(data))
    .catch((e) => {
      console.log(e);
      console.log(submitUrl);
      console.log(executorUrl);
    });
};

/**
 * remove job with specified id from job manager
 * @param submitUrl
 * @param executorUrl
 * @param jobID
 */
export const killJob = (submitUrl, executorUrl, jobID) => {
  axios
    .post(
      executorUrl,
      {
        type: "salsa",
        subtype: "feeder",
        salsa: { host: submitUrl, job_type: "command" },
        command: "JOB_DEL_ID",
        args: jobID,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((data) => console.log(data))
    .catch((e) => {
      console.log(e);
      console.log(submitUrl);
      console.log(executorUrl);
    });
};

/**
 * remove all finished jobs from job manager
 * @param submitUrl
 * @param executorUrl
 */
export const clearFinished = (submitUrl, executorUrl) => {
  axios
    .post(
      executorUrl,
      {
        type: "salsa",
        subtype: "feeder",
        salsa: { host: submitUrl, job_type: "command" },
        command: "JOB_DEL_FINISHED",
        args: "",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((data) => console.log(data))
    .catch((e) => {
      console.log(e);
      console.log(submitUrl);
      console.log(executorUrl);
    });
};
