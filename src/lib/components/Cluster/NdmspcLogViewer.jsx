import React, { useState, useEffect, useContext } from 'react'
import axios from 'axios';
import { useNdmSpcLocal, useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';
import { LogViewer, LogViewerSearch } from '@patternfly/react-log-viewer';
import { Button, Icon, Label, Modal, Stack, StackItem, Toolbar, ToolbarContent, ToolbarItem } from '@patternfly/react-core';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon';
import CheckCircleIcon from '@patternfly/react-icons/dist/esm/icons/check-circle-icon';



export const NdmspcLogViewer = ({ logs = null, basedir = "https://eos.ndmspc.io/eos/ndmspc/scratch/ndmspc/logs/", logfile = "ndmspc.log" }) => {

  const [activeItem, setActiveItem] = React.useState(0);
  const [variant] = useState()
  const [content, setContent] = useState("")
  useEffect(() => {
    const file = `${basedir}/${logs.mgr}/${logs.job.name}/${activeItem}/${logfile}`
    // console.log(file)
    axios.get(file)
      .then(res => {
        const data = res.data;
        // console.log(data)
        setContent(data);
      }).catch(err => {
        setContent(err)
      })
  }, [activeItem]);

  return logs ? (
    <>
      <Stack>
        <StackItem>
          {logs.job.rc.failed.map((id) => {
            return <Label status="danger" key={`${logs.job.name}-${id}-failed`} icon={<ExclamationCircleIcon />} color="red" variant={id == activeItem ? 'filled' : 'outline'} onClick={() => setActiveItem(id)}>
              {id}
            </Label>
          })
          }
          {
            logs.job.rc.done.map((id) => {
              return <Label status="danger" key={`${logs.job.name}-${id}-failed`} icon={<CheckCircleIcon />} color="green" variant={id == activeItem ? 'filled' : 'outline'} onClick={() => setActiveItem(id)}>
                {id}
              </Label>
            })
          }</StackItem>
        <StackItem><Label>curl -L {basedir}/{logs.mgr}/{logs.job.name}/{activeItem}/{logfile}</Label></StackItem>
        <StackItem isFilled>
          <LogViewer
            hasLineNumbers={false}
            height={600}
            data={content || ""}
            theme={'light'}
            toolbar={
              <Toolbar>
                <ToolbarContent>
                  <ToolbarItem>
                    <LogViewerSearch placeholder="Search value" />
                  </ToolbarItem>
                </ToolbarContent>
              </Toolbar>
            }
          />

        </StackItem>
      </Stack>
    </>
  ) : null
}

export const NdmspcLogViewerModal = ({ title = "NDMSPC Log Viewer" }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [ndmspcInternal,] = useNdmSpcRedirect("ndmspcInternal");
  const [logs, setLogs] = useState(null)

  useEffect(() => {
    if (!ndmspcInternal) return

    if (ndmspcInternal?.type == "logs" && ndmspcInternal?.action == "open") {
      setLogs(ndmspcInternal?.payload)
      setIsModalOpen(true)
    } else if (ndmspcInternal?.type == "logs" && ndmspcInternal?.action == "close") {
      setLogs(null)
      setIsModalOpen(false)
    }
    // console.log(ndmspcInternal)
  }, [ndmspcInternal]);


  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };
  return (
    <Modal position="top" width="80%" title={title} isOpen={isModalOpen} onClose={handleModalToggle} >
      <NdmspcLogViewer logs={logs} />
    </Modal>
  )
}


