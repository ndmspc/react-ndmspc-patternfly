import React, { useEffect, useState } from 'react';
import { NavExpandable, NavItem, ToolbarGroup, ToolbarItem } from '@patternfly/react-core';
import { useNdmSpcPlugin, useNdmSpcRedirect, useNdmSpcLocal } from "@ndmspc/react-ndmspc-core";
import { applyPluginConfig } from '../../../utils/core';
// import { ProjectionSelect } from './ProjectionSelect';
export const ProjectionNavGroup = ({ groupId = "nav-group-projection" }) => {
  const [activeGroup, setActiveGroup] = React.useState('');
  const [activeItem, setActiveItem] = React.useState('');


  const [analysis, setAnalysis] = useState(null);


  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const [ndmspcConfiguration, ndmspcConfigurationStore] = useNdmSpcRedirect("ndmspcConfiguration");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()


  const onMenuClick = (item) => {
    // console.log("ProjectionNavGroup::onMenuClick ", item)
    if (!analysis) return
    ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, analyses: { ...ndmspcAnalyses.analyses, [analysis.name]: { ...ndmspcAnalyses?.analyses[analysis.name], projection: item } } });
  };

  useEffect(() => {
    if (!ndmspcInternal) return

    if (ndmspcInternal.type === "nav" && ndmspcInternal.action === "click") {
      // console.log("PluginNavGroup::click ", ndmspcInternal)
      if (ndmspcInternal.payload.group === groupId) {
        setActiveGroup(ndmspcInternal.payload.group)
        setActiveItem(ndmspcInternal.payload.item)
        onMenuClick(ndmspcInternal.payload.item)
      } else {
        setActiveGroup(ndmspcInternal.payload.group)
        setActiveItem('')
      }
    }
  }, [ndmspcInternal]);


  useEffect(() => {
    // console.log(ndmspcAnalyses)
    if (ndmspcAnalyses && ndmspcAnalyses?.analyses && ndmspcAnalyses?.analysis) {
      setAnalysis(ndmspcAnalyses.analyses[ndmspcAnalyses.analysis])
    } else {
      setAnalysis(null)
    }

  }, [ndmspcAnalyses]);

  useEffect(() => {
    if (!analysis) return
    // if (!analysis?.projection) return
    console.log("ProjectionNavGroup::analysis: ", analysis, analysis?.projection)
    setActiveItem(analysis?.projection)
  }, [analysis]);


  useEffect(() => {
    if (!activeItem) return
    if (!ndmspcCurrentPlugin) return
    if (!ndmspcConfiguration) return
    const pluginName = ndmspcCurrentPlugin.state.name
    if (pluginName.length === 0) return
    if (!ndmspcConfiguration[pluginName]) return
    let cfg = null;
    if (ndmspcConfiguration[pluginName]) {
      cfg = ndmspcConfiguration[pluginName][ndmspcConfiguration[pluginName].current].config
      cfg.name = ndmspcConfiguration[pluginName].current
    }
    if (!cfg) return
    const analysis = ndmspcAnalyses.analyses[ndmspcAnalyses.analysis]
    const projection = analysis.projections[activeItem]
    applyPluginConfig(cfg, projection, pluginName, ndmspcConfiguration, ndmspcConfigurationStore)
  }, [activeItem, ndmspcCurrentPlugin]);




  return analysis ? (
    <NavExpandable title="Projections" groupId={groupId} isActive={false} isExpanded={true}>
      {analysis?.projections ?
        Object.keys(analysis?.projections).map((proj) =>
        (
          <NavItem preventDefault key={`${groupId}-${proj}-item`} id={`${groupId}-${proj}-item`} groupId={groupId} itemId={`${proj}`} isActive={activeItem === proj}>
            {proj}
          </NavItem>
        )) : null}
    </NavExpandable>) : null
};