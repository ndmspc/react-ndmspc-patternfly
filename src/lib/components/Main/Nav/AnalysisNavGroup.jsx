import React, { useEffect, useState } from 'react';
import { Button, Icon, NavExpandable, NavItem, ToolbarGroup, ToolbarItem } from '@patternfly/react-core';
import TimesIcon from '@patternfly/react-icons/dist/js/icons/times-icon';
import PlusCircleIcon from '@patternfly/react-icons/dist/esm/icons/plus-circle-icon';
import UploadIcon from '@patternfly/react-icons/dist/esm/icons/upload-icon';
import { useNdmSpcLocal, useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';
export const AnalysisNavGroup = ({ groupId = "nav-group-analysis" }) => {

  const [activeGroup, setActiveGroup] = React.useState('');
  const [activeItem, setActiveItem] = React.useState('');
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");

  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const [analysis, setAnalysis] = useState(null);



  const onClickProfileDeselect = () => {
    ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, analysis: "" });
  };

  const onClickProfileExport = () => {
    const fileData = JSON.stringify(ndmspcAnalyses);
    const blob = new Blob([fileData], { type: "text/plain" });
    const url = URL.createObjectURL(blob);
    const link = document.createElement("a");
    const date = new Date();
    link.download = `ndmspc-${date.getFullYear()}${date.getMonth() + 1}${date.getDate()}-${date.getHours()}${date.getMinutes()}${date.getSeconds()}.ndmspc`;
    link.href = url;
    link.click();
  };



  const onMenuClick = (item) => {
    switch (item) {
      case 'close':
        onClickProfileDeselect()
        break;
      case 'export':
        onClickProfileExport()
        break;
    }
  };

  useEffect(() => {
    if (!ndmspcInternal) return

    // ndmspcInternal.filter(a => a !== current)
    if (ndmspcInternal.type === "nav" && ndmspcInternal.action === "click") {
      // console.log("AnalysisNavGroup::click ", ndmspcInternal)
      if (ndmspcInternal.payload.group === groupId) {
        setActiveGroup(ndmspcInternal.payload.group)
        setActiveItem(ndmspcInternal.payload.item)
        onMenuClick(ndmspcInternal.payload.item)
      } else {
        setActiveGroup(ndmspcInternal.payload.group)
        setActiveItem('')
      }
    }
  }, [ndmspcInternal]);

  useEffect(() => {
    if (!ndmspcAnalyses) return
    if (!ndmspcAnalyses?.analyses && ndmspcAnalyses?.analysis?.length > 0) {
      setAnalysis(ndmspcAnalyses.analyses[ndmspcAnalyses.analysis])
    } else {
      setAnalysis(null)
    }
  }, [ndmspcAnalyses]);

  return (
    <NavExpandable title="Analysis" groupId={groupId} isActive={activeGroup === groupId} isExpanded={true}>
      <NavItem preventDefault id="export" to="#export" groupId={groupId} itemId="export" isActive={activeItem === 'export'}>
        Export
      </NavItem>
      <NavItem preventDefault id="close" to="#close" groupId={groupId} itemId="close" isActive={activeItem === 'close'}>
        Close
      </NavItem>
    </NavExpandable>
  );
};  