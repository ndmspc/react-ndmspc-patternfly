import React, { useEffect, useState } from 'react';
import { NavExpandable, NavItem, Spinner, ToolbarGroup, ToolbarItem } from '@patternfly/react-core';
import { useNdmSpcPlugins, useNdmSpcPlugin, useNdmSpcRedirect, useNdmSpcLocal } from "@ndmspc/react-ndmspc-core";

export const PluginNavItem = ({ groupId = "nav-group-plugin-EmptyPlugin", name = "", tool = null, histogramDimension = 1 }) => {
  // const [activeGroup, setActiveGroup] = React.useState('');
  // const [activeItem, setActiveItem] = React.useState(null);
  // const [activePlugin, setActivePlugin] = React.useState(null);
  const [activeTool, setActiveTool] = React.useState(null);

  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  // const onPluginChangeClick = (item) => {
  //   // console.log("PluginNavItem::onPluginChangeClick ", item)
  //   ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, plugin: item });
  // };


  const onMenuClick = (type, value) => {
    // console.log("PluginNavItem::onMenuClick ", type, value)
    switch (type) {
      case 'bin':
        ndmspcCurrentPlugin.getDistributorById("selectedPluginTool")?.sendInputData({ name: type, value });
        // setActiveItem("bin/single")
        break;
      case 'refresh':
        if (!ndmspcAnalyses) return
        ndmspcCurrentPlugin.userFunctions?.onScanHistogram({ analysis: ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis], binPresent: 2, binMissing: value === "prod" ? 0 : 1 })
        ndmspcInternalStore.setData({
          type: "nav",
          action: "click",
          payload: {
            group: groupId,
            item: "bin/single"
          }
        })


        break;
      case 'configuration':
        ndmspcAppStore.setData({ ...ndmspcApp, isPluginConfigurationOpen: true })
        break;

    }
  };


  // useEffect(() => {
  //   if (!ndmspcAnalyses) return
  //   if (!ndmspcAnalyses?.analyses) return
  //   if (!ndmspcAnalyses?.analysis) return
  //   const projection = ndmspcAnalyses?.analyses[ndmspcAnalyses?.analysis]?.projection
  //   if (projection)
  //     setHistogramDimension(projection.split("_").length)

  // }, [ndmspcAnalyses]);

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    // console.log("PluginNavItem::currentPlugin::plugin", name, ndmspcCurrentPlugin)
    if (ndmspcCurrentPlugin?.state?.name === name) {
      // console.log("PluginNavItem::currentPlugin::name", name, ndmspcCurrentPlugin?.state?.name)
      // let activeValues = {};
      // const tools = ndmspcCurrentPlugin?.state?.tools
      // console.log("ndmspcCurrentPlugin: ", ndmspcCurrentPlugin?.state?.tools)
      // if (tools) {
      //   Object.keys(tools).map((tool) => {
      //     console.log(tools[tool]?.value)
      //     activeValues = { ...activeValues, [tool]: tools[tool]?.value }      `                                               `
      //   })
      //   console.log("ndmspcCurrentPlugin: setActiveItem", activeValues)
      //   setActiveItem(activeValues)
      // }

      // setActivePlugin(name)
      // setActiveItem(`${name}/`)
    } else {
      // setActivePlugin(null)
    }
  }, [ndmspcCurrentPlugin]);


  useEffect(() => {
    if (!ndmspcInternal) return
    if (ndmspcInternal.type === "nav" && ndmspcInternal.action === "click") {
      // console.log("PluginNavItem::click ", ndmspcInternal, groupId)
      if (ndmspcInternal.payload.group === groupId) {
        console.log("PluginNavItem::click-this ", ndmspcInternal.payload.group, ndmspcInternal.payload.item, activeTool)
        const item = ndmspcInternal.payload.item.split('/')
        console.log(item)
        if (name === item[0]) {
          onMenuClick(item[0], item[1])
          setActiveTool({ ...activeTool, value: item[1] })
        }

        // setActiveGroup(ndmspcInternal.payload.group)
        // const item = ndmspcInternal.payload.item.split('/')
        // console.log(item)
        // const o = { ...activeItem, [item[0]]: item[1] }
        // console.log(o)
        // setActiveItem(o)
        // console.log("o2", o)
        // // item.split('/')[0], value: item.split('/')[1]
        // onMenuClick(ndmspcInternal.payload.item)
      } else {
        // console.log("PluginNavItem::click-other ", groupId, ndmspcInternal)
        //       // setActiveGroup('')
        // setActiveItem('')
      }
    } else if (ndmspcInternal.type === "nav" && ndmspcInternal.action === "toggle") {
      // console.log("PluginNavItem::toggle ", ndmspcInternal, ndmspcCurrentPlugin?.state?.name)
      if (ndmspcInternal.payload.expanded === true && ndmspcInternal.payload.group === groupId) {
        // console.log("PluginNavItem::toggle Own tools", ndmspcInternal, name, ndmspcCurrentPlugin?.state?.name, activePlugin)
        // if (name !== ndmspcCurrentPlugin?.state?.name && name === ndmspcInternal.payload.group.split('-').pop()) {
        //   onPluginChangeClick(name)
        // }
      }
    } else if (ndmspcInternal?.type === "job" && ndmspcInternal?.action === "redraw" && ndmspcInternal?.bin === "all") {
      // console.log("PluginNavItem::refresh ", ndmspcInternal)

      ndmspcInternalStore.setData({
        type: "nav",
        action: "click",
        payload: {
          group: groupId,
          item: "refresh/prod"
        }
      })

      // onMenuClick("refresh", "dev")


      // if (ndmspcCurrentPlugin.userFunctions?.onScanHistogram && ndmspcAnalyses?.analyses) ndmspcCurrentPlugin.userFunctions?.onScanHistogram({ analysis: ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis] })
    }
  }, [ndmspcInternal]);

  // useEffect(() => {
  // if (!activePlugin) return;
  // console.log("PluginNavItem::activePlugin ", name, activeTool)
  // setPluginTools(ndmspcCurrentPlugin?.state?.tools)
  // }, [name]);

  useEffect(() => {
    if (!tool) return;
    // console.log("PluginNavItem::tool ", name, tool)
    setActiveTool(tool)
  }, [tool]);

  useEffect(() => {
    if (!activeTool) return;
    // console.log("PluginNavItem::activeTool ", name, activeTool)
  }, [activeTool]);


  if (activeTool) {
    if (activeTool.type === "select") {
      return (
        <NavExpandable title={activeTool.title} key={`${groupId}-${name}-exp`} groupId={`${groupId}-${name}-exp`} isActive={false} isExpanded={true}>
          {
            activeTool.options[histogramDimension] ? activeTool.options[histogramDimension].map((opt) => {
              // console.log("PluginNavItem::return ", name, activeTool.value, opt)
              return (
                <NavItem preventDefault key={`${groupId}-${name}-${opt}-item`} id={`${groupId}-${name}-${opt}-item`} groupId={groupId} itemId={`${name}/${opt}`} isActive={activeTool.value === opt}>
                  {opt}
                </NavItem>
              )
            }
            ) : null
          }
        </NavExpandable>
      )
    } else if (activeTool.type === "button") {
      return (
        <NavItem preventDefault key={`${groupId}-${name}-item`} id={`${groupId}-${name}-item`} groupId={groupId} itemId={`${name}`} isActive={false}>
          {activeTool.title}
        </NavItem>
      )
    } else {

      return (
        <NavItem preventDefault key={`${groupId}-${name}-item-null`} >
          Not supported ({name})
          {/* <PluginOptionSelect key={`${tool}-tool`} name={tool} title={pluginTools[tool].title} type={pluginTools[tool].type} icon={pluginTools[tool]?.icon} options={pluginTools[tool].options} histDim={histogramDimension} /> */}
        </NavItem>
      )

    }
  } else {
    return null
  }

};








export const PluginNavGroup = ({ groupId = "nav-group-plugin-EmptyPlugin", name = "" }) => {
  const [activeGroup, setActiveGroup] = React.useState('');
  const [activeItem, setActiveItem] = React.useState(null);
  const [activePlugin, setActivePlugin] = React.useState(null);
  const [pluginTools, setPluginTools] = useState(null);
  const [histogramDimension, setHistogramDimension] = useState(1);

  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  const onPluginChangeClick = (item) => {
    // console.log("PluginNavGroup::onPluginChangeClick ", item)
    ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, plugin: item });
  };


  // const onMenuClick = (item) => {
  //   switch (item) {
  //     case 'bin/single':
  //     case 'bin/all':
  //       ndmspcCurrentPlugin.getDistributorById("selectedPluginTool")?.sendInputData({ name: item.split('/')[0], value: item.split('/')[1] });
  //       // setActiveItem("bin/single")
  //       break;
  //     case 'refresh/dev':
  //     case 'refresh/prod':
  //       if (!ndmspcAnalyses) return
  //       ndmspcCurrentPlugin.userFunctions?.onScanHistogram({ analysis: ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis], binPresent: 2, binMissing: item.split('/')[1] === "prod" ? 0 : 1 })
  //       setActiveItem({ ...activeItem, bin: "single" })
  //       onMenuClick("bin/single")
  //       break;
  //     case 'configuration':
  //       ndmspcAppStore.setData({ ...ndmspcApp, isPluginConfigurationOpen: true })
  //       break;

  //   }
  // };


  // useEffect(() => {
  //   if (!ndmspcAnalyses) return
  //   if (!ndmspcAnalyses?.analyses) return
  //   if (!ndmspcAnalyses?.analysis) return
  //   const projection = ndmspcAnalyses?.analyses[ndmspcAnalyses?.analysis]?.projection
  //   if (projection)
  //     setHistogramDimension(projection.split("_").length)

  // }, [ndmspcAnalyses]);

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return

    if (ndmspcCurrentPlugin?.state?.name === name) {
      setActivePlugin(name)
    } else {
      setActivePlugin(null)
    }
    //     // console.log("currentPlugin::tools", name, ndmspcCurrentPlugin?.state?.name)
    //     let activeValues = {};
    //     const tools = ndmspcCurrentPlugin?.state?.tools
    //     console.log("ndmspcCurrentPlugin: ", ndmspcCurrentPlugin?.state?.tools)
    //     if (tools) {
    //       Object.keys(tools).map((tool) => {
    //         console.log(tools[tool]?.value)
    //         activeValues = { ...activeValues, [tool]: tools[tool]?.value }      `                                               `
    //       })
    //       console.log("ndmspcCurrentPlugin: setActiveItem", activeValues)
    //       setActiveItem(activeValues)
    //     }
    //   } 
  }, [ndmspcCurrentPlugin]);


  useEffect(() => {
    if (!ndmspcInternal) return
    if (ndmspcInternal.type === "nav" && ndmspcInternal.action === "click") {
      // console.log("PluginNavGroup::click ", ndmspcInternal, groupId)
      if (ndmspcInternal.payload.group === groupId) {
        // console.log("PluginNavGroup::click-this ", ndmspcInternal.payload.group, ndmspcInternal.payload.item)
        //       setActiveGroup(ndmspcInternal.payload.group)
        //       const item = ndmspcInternal.payload.item.split('/')
        //       console.log(item)
        //       const o = { ...activeItem, [item[0]]: item[1] }
        //       console.log(o)
        //       setActiveItem(o)
        //       console.log("o2", o)
        //       // item.split('/')[0], value: item.split('/')[1]
        //       onMenuClick(ndmspcInternal.payload.item)
        //     } else {
        //       // console.log("PluginNavGroup::click-other ", groupId, ndmspcInternal)
        //       //       // setActiveGroup('')
        //       // setActiveItem('')
      }
    } else if (ndmspcInternal.type === "nav" && ndmspcInternal.action === "toggle") {
      // console.log("PluginNavGroup::toggle ", ndmspcInternal, ndmspcCurrentPlugin?.state?.name)
      if (ndmspcInternal.payload.expanded === true && ndmspcInternal.payload.group === groupId) {
        // console.log("PluginNavGroup::toggle Own tools", ndmspcInternal, name, ndmspcCurrentPlugin?.state?.name, activePlugin)
        if (name !== ndmspcCurrentPlugin?.state?.name && name === ndmspcInternal.payload.group.split('-').pop()) {
          onPluginChangeClick(name)
        }
      }
    } else if (ndmspcInternal?.type === "job" && ndmspcInternal?.action === "redraw" && ndmspcInternal?.bin === "all") {
      //     console.log("PluginNavGroup::refresh ", ndmspcInternal)
      //     onMenuClick("refresh")


      //     // if (ndmspcCurrentPlugin.userFunctions?.onScanHistogram && ndmspcAnalyses?.analyses) ndmspcCurrentPlugin.userFunctions?.onScanHistogram({ analysis: ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis] })
    }
  }, [ndmspcInternal]);


  useEffect(() => {
    if (!activePlugin) return;
    // console.log("PluginNavGroup::activePlugin ", name, activePlugin, ndmspcCurrentPlugin?.state?.tools)
    if (name === activePlugin) {
      setPluginTools(ndmspcCurrentPlugin?.state?.tools)
    }
  }, [activePlugin]);

  // useEffect(() => {
  //   if (!pluginTools) return;
  //   console.log("PluginNavGroup::pluginTools ", name, pluginTools)
  // }, [pluginTools]);



  return name ? (
    <NavExpandable title={name} key={`${groupId}-exp`} groupId={`${groupId}`} isActive={activePlugin === name} isExpanded={activePlugin === name}>
      {pluginTools ? Object.keys(pluginTools).map((tool) => {
        return (
          <PluginNavItem key={`${groupId}-${tool}-exp`} groupId={groupId} name={tool} tool={pluginTools[tool]} histogramDimension={histogramDimension} />
        )
      }) : null
      }

    </NavExpandable>
  ) : <Spinner />
};


export const PluginNavGroups = ({ groupId = "nav-group-plugin" }) => {
  const ndmspcPlugins = useNdmSpcPlugins()

  // useEffect(() => {
  //   if (!ndmspcPlugins) return;
  //   console.log("PluginNavGroups::ndmspcPlugins ", ndmspcPlugins)
  // }, [ndmspcPlugins]);

  return ndmspcPlugins ? Object.keys(ndmspcPlugins).map((option) => <PluginNavGroup key={`${groupId}-${option}`} groupId={`${groupId}-${option}`} name={option} />) : null
};