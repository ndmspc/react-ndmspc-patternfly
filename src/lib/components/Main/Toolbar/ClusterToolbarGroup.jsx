import React, { useEffect, useState } from 'react';
import { ToolbarGroup, ToolbarItem, Button, Icon } from '@patternfly/react-core';
import ClusterIcon from '@patternfly/react-icons/dist/esm/icons/cluster-icon';
import BellIcon from '@patternfly/react-icons/dist/js/icons/bell-icon';
import TrashIcon from '@patternfly/react-icons/dist/js/icons/trash-icon';


import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';
export const ClusterToolbarGroup = () => {
  const [clusterColor, setClusterColor] = useState("danger")
  const [subscribedColor, setSubscribedColor] = useState("danger")
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws,] = useNdmSpcLocal("ndmspcZmq2ws");

  const onClickZmq2wsOpen = () => {
    if (!ndmspcApp) return
    if (ndmspcApp?.isZmq2wsOpen)
      ndmspcAppStore.setData({ ...ndmspcApp, isZmq2wsOpen: !ndmspcApp.isZmq2wsOpen });
    else
      ndmspcAppStore.setData({ ...ndmspcApp, isZmq2wsOpen: true });

  };

  const onReset = () => {
    localStorage.clear();
    window.location.reload(true);
  };

  useEffect(() => {
    if (!ndmspcZmq2ws) return
    setClusterColor(ndmspcZmq2ws.connected ? "success" : "danger")
    setSubscribedColor(ndmspcZmq2ws.subscribed ? "success" : "danger")
  }, [ndmspcZmq2ws]);

  return (
    <>
      <ToolbarItem>
        <Button variant="plain" aria-label="cluster" onClick={onClickZmq2wsOpen}>
          <Icon status={clusterColor}><ClusterIcon /></Icon>
        </Button>
      </ToolbarItem>
      <ToolbarItem>
        <Button variant="plain" aria-label="subscription" onClick={onClickZmq2wsOpen}>
          <Icon status={subscribedColor} ><BellIcon /></Icon>
        </Button>
      </ToolbarItem>
      <ToolbarItem>
        <Button variant="plain" aria-label="reset" onClick={onReset}>
          <Icon status="info" ><TrashIcon /></Icon>
        </Button>
      </ToolbarItem>
    </>);
};