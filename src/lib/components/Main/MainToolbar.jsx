import React from 'react';
import { ToolbarGroup, Toolbar, ToolbarContent, ToolbarItem, Button, ButtonVariant } from '@patternfly/react-core';
import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';
import CogIcon from '@patternfly/react-icons/dist/esm/icons/cog-icon';
import { ClusterToolbarGroup } from './Toolbar/ClusterToolbarGroup';
export const MainToolbar = () => {

  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");

  const onConfigOpen = () => {
    if (!ndmspcApp) return

    console.log("MainToolbar:", ndmspcApp)
    if (ndmspcApp?.isZmq2wsOpen)
      ndmspcAppStore.setData({ ...ndmspcApp, isMainConfigurationOpen: !ndmspcApp.isMainConfigurationOpen });
    else
      ndmspcAppStore.setData({ ...ndmspcApp, isMainConfigurationOpen: true });
  };

  return (
    <Toolbar id="main-toolbar" isFullHeight isStatic>
      <ToolbarContent>
      <ToolbarGroup variant="icon-button-group" align={{
          default: 'alignRight'
        }} spacer={{
          default: 'spacerNone',
          md: 'spacerMd'
        }}>
          <ClusterToolbarGroup />
          <ToolbarItem>
            <Button aria-label="Settings" variant={ButtonVariant.plain} icon={<CogIcon />} onClick={onConfigOpen} />
          </ToolbarItem>
        </ToolbarGroup>
      </ToolbarContent>
    </Toolbar>
  );
};