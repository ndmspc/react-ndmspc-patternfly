import React from 'react';
import { Button, FileUpload } from '@patternfly/react-core';
import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';
export const NdmspcConfigLoad = () => {


  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");

  const [value, setValue] = React.useState('');
  const [filename, setFilename] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const handleFileInputChange = (_, file) => {
    setFilename(file.name);
  };
  const handleTextChange = (_event, value) => {
    setValue(value);
  };
  const handleDataChange = (_event, value) => {
    setValue(value);
  };
  const handleClear = _event => {
    setFilename('');
    setValue('');
  };
  const handleFileReadStarted = (_event, _fileHandle) => {
    setIsLoading(true);
  };
  const handleFileReadFinished = (_event, _fileHandle) => {
    setIsLoading(false);
  };


  const handleApply = () => {
      let output = JSON.parse(value);
      ndmspcAnalysesStore.setData(output);
  };

  return (<>
    <FileUpload id="text-file-simple" type="text" value={value} filename={filename} filenamePlaceholder="Drag and drop a file or upload one" onFileInputChange={handleFileInputChange} onDataChange={handleDataChange} onTextChange={handleTextChange} onReadStarted={handleFileReadStarted} onReadFinished={handleFileReadFinished} onClearClick={handleClear} isLoading={isLoading} allowEditingUploadedText={false} browseButtonText="Upload" />
    <Button variant="primary" onClick={handleApply} isAriaDisabled={value.length == 0}>Apply</Button>
  </>)
};