import React, { useState, useEffect } from "react";
import { useNdmSpcLocal, useNdmSpcPlugin, useNdmSpcRedirect } from "@ndmspc/react-ndmspc-core";
import { NdmspcWorkspace } from "../Workspace/NdmspcWorkspace";
import { ProjectionInit } from "../Analysis/ProjectionInit";
import { readJsrootObjectFromFile } from "@ndmspc/react-ndmspc-core";
import { applyConfigFromAnalysis } from "../../utils/core";

export const NdmspcMainContent = ({ name = "zmq2ws" }) => {
  const [once, setOnce] = useState(true)
  const [currentNdmspcHistogram, setCurrentNdmspcHistogram] = useState(null)
  const [currentAnalysis, setCurrentAnalysis] = useState(null)
  const [currentProjection, setCurrentProjection] = useState(null)
  const [ndmspcAnalyses,] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcInternal,] = useNdmSpcRedirect("ndmspcInternal");
  const [ndmspcConfiguration, ndmspcConfigurationStore] = useNdmSpcRedirect("ndmspcConfiguration");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()




  const onInitDone = () => {
    if (currentProjection) {
      readJsrootObjectFromFile(currentProjection.file, currentProjection.obj, setCurrentNdmspcHistogram, setCurrentNdmspcHistogram).catch((e) => console.log(e));
    }
  };

  useEffect(() => {
    if (ndmspcAnalyses) {
      setCurrentAnalysis(ndmspcAnalyses.analyses[ndmspcAnalyses.analysis])
      // console.log("AAAAAAAAAAAAAAAAAA ", ndmspcConfiguration)
      // if (ndmspcConfiguration)
      //   applyConfigFromAnalysis({}, ndmspcAnalyses.analyses[ndmspcAnalyses.analysis], ndmspcConfiguration, ndmspcConfigurationStore, false)
    }
  }, [ndmspcAnalyses]);

  useEffect(() => {
    if (currentAnalysis) {
      const projection = currentAnalysis.projections[currentAnalysis.projection]
      setCurrentProjection(projection)
    }
  }, [currentAnalysis]);

  useEffect(() => {
    if (currentProjection) {
      readJsrootObjectFromFile(currentProjection.file, currentProjection.obj, setCurrentNdmspcHistogram, setCurrentNdmspcHistogram).catch((e) => console.log(e));
    }
  }, [currentProjection]);

  useEffect(() => {
    if (!ndmspcInternal) return
    if (ndmspcInternal?.type === "job" && ndmspcInternal?.action === "redraw" && ndmspcInternal?.canvas === "workspace-main" && currentProjection?.file) {
      // console.log("pluginPipe: Updating histogram", ndmspcInternal?.filename, ndmspcInternal?.canvas)
      readJsrootObjectFromFile(currentProjection.file, currentProjection.obj, setCurrentNdmspcHistogram, setCurrentNdmspcHistogram).catch((e) => console.log(e));
    }
    // else if (ndmspcInternal?.type === "job" && ndmspcInternal?.action === "redraw" && ndmspcInternal?.bin === "all") {
    //   if (ndmspcCurrentPlugin.userFunctions?.onScanHistogram) ndmspcCurrentPlugin.userFunctions?.onScanHistogram({ analysis: currentAnalysis })
    // }

  }, [ndmspcInternal]);

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    const subscriptionPluginPipe = ndmspcCurrentPlugin.createExternalSubscription(
      "pluginPipe",
      (data) => {
        if (!data) return
        if (data?.type === "histogram" && data?.action === "redraw") {
          console.log("pluginPipe: Updating histogram", data?.payload)
          setCurrentNdmspcHistogram({ ...data?.payload, timestamp: new Date() })

        }
      },
      null
    );
    return () => {
      subscriptionPluginPipe?.unsubscribe();
    }
  }, [ndmspcCurrentPlugin]);


  // useEffect(() => {
  //   if (once) return
  //   if (!ndmspcConfiguration) return
  //   setOnce(true)
  //   console.log("AAAAAAAAAAAAAAAA",ndmspcConfiguration)
  //   if (ndmspcConfiguration)
  //     applyConfigFromAnalysis({}, ndmspcAnalyses.analyses[ndmspcAnalyses.analysis], ndmspcConfiguration, ndmspcConfigurationStore, false)

  // }, [once,ndmspcConfiguration]);



  // currentNdmspcHistogram

  return currentNdmspcHistogram ?
    (<NdmspcWorkspace name={name} histogram={currentNdmspcHistogram} />) :
    (<ProjectionInit analysis={currentAnalysis} projection={currentProjection} initDone={onInitDone} />)
};
