import React, { useEffect, useState } from 'react';
import { Button, EmptyState, EmptyStateBody, EmptyStateActions, EmptyStateHeader, EmptyStateFooter, EmptyStateIcon, Modal, ModalVariant, PageSection, PageSectionVariants, Card, Bullseye, EmptyStateVariant, Page, FileUpload } from '@patternfly/react-core';
import CubesIcon from '@patternfly/react-icons/dist/esm/icons/cubes-icon';
import PlusCircleIcon from '@patternfly/react-icons/dist/esm/icons/plus-circle-icon';
import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';
import { InitHistogramWizard } from '../Wizards/InitHistogramWizard';
import { NdmspcAnalyses } from '../Analysis/Analyses';
import { NdmspcConfigLoad } from './NdmspcConfigLoad';
export const NdmspcWelcome = ({defaultAnalysisName="myAnalysis"}) => {

  const [ndmspcAnalyses] = useNdmSpcLocal("ndmspcAnalyses");
  const [analysisName, setAnalysisName] = useState(defaultAnalysisName)
  const [analyses, setAnalyses] = useState([]);
  const [isAnalysisModalOpen, setIsAnalysisModalOpen] = useState(false);
  const [isConfigUploadModalOpen, setIsConfigUploadModalOpen] = useState(false);


  const handleAnalysisModalToggle = () => setIsAnalysisModalOpen(!isAnalysisModalOpen);
  const handleConfigUploadModalToggle = () => setIsConfigUploadModalOpen(!isConfigUploadModalOpen);

  useEffect(() => {
    setAnalyses(ndmspcAnalyses?.analyses)
    // console.log(ndmspcAnalyses?.analyses)
  }, [ndmspcAnalyses]);

  const onNewAnalysis = () => {
    setAnalysisName(defaultAnalysisName)
    setIsAnalysisModalOpen(true);
  };

  const onNewProjection = (analysisName) => {
    console.log("onNewProjection", analysisName)
    // setAnalyses(ndmspcAnalyses?.analyses)
    setAnalysisName(analysisName)
    setIsAnalysisModalOpen(true);
  };

  const onLoadConfig = () => {
    console.log("Loading config")
    setIsConfigUploadModalOpen(true);
  };


  const EmptyAnalysis = (
    <Card isCompact onClick={onNewAnalysis}>
      <Bullseye>
        <EmptyState variant={EmptyStateVariant.xs}>
          <EmptyStateHeader headingLevel="h2" titleText="Add analysis profile" icon={<EmptyStateIcon icon={PlusCircleIcon} />} />
          <EmptyStateFooter>
            <EmptyStateActions>
              {/* <Button variant="link" onClick={onNewAnalysis}>Add new</Button> */}
            </EmptyStateActions>
          </EmptyStateFooter>
        </EmptyState>
      </Bullseye>
    </Card>
  )

  return (
    <Page>
      <PageSection variant={PageSectionVariants.light}>
        <EmptyState>
          <EmptyStateHeader titleText="Welcome to NDMSPC (N DiMensional SPaCe) !!!" headingLevel="h4" icon={<EmptyStateIcon icon={CubesIcon} />} />
          <EmptyStateBody>
            Start analysis by creating or loading analysis profile.
          </EmptyStateBody>
          <EmptyStateFooter>
            <EmptyStateActions>
              <Button variant="primary" onClick={onNewAnalysis}>Create</Button>
              <Button variant="primary" onClick={onLoadConfig}>Load</Button>
            </EmptyStateActions>
          </EmptyStateFooter>

          <Modal
            isOpen={isAnalysisModalOpen}
            onClose={handleAnalysisModalToggle}
            variant={ModalVariant.large}
            aria-labelledby="wiz-modal-title-init"
          >
            <InitHistogramWizard onClose={handleAnalysisModalToggle} defaultAnalysisName={analysisName}/>
          </Modal>
          <Modal
            isOpen={isConfigUploadModalOpen}
            onClose={handleConfigUploadModalToggle}
            variant={ModalVariant.large}
            aria-labelledby="wiz-modal-title-init"
          >
            <NdmspcConfigLoad />
          </Modal>


        </EmptyState>
      </PageSection>
      <PageSection aria-label="page-section-analyses" isFilled isCenterAligned hasShadowTop hasShadowBottom hasOverflowScroll>
        <NdmspcAnalyses empty={EmptyAnalysis} analyses={analyses} onNewProjection={onNewProjection}/>
      </PageSection>
    </Page >
  );
};