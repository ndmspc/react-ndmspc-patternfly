import React, { useContext, useEffect } from "react";
import { NdmSpcContext } from "@ndmspc/react-ndmspc-core";
import { Masthead, MastheadMain, MastheadBrand, MastheadContent, MastheadToggle, Button, Brand, Page, PageSidebar, PageSidebarBody, NavList, NavItem, Nav, PageToggleButton, SkipToContent, PageSectionVariants, PageSection, TextContent, Text, Breadcrumb, BreadcrumbItem, NavExpandable } from '@patternfly/react-core';
import BarsIcon from '@patternfly/react-icons/dist/js/icons/bars-icon';
import { useNdmSpcLocal, useNdmSpcRedirect } from "@ndmspc/react-ndmspc-core";
import { NdmspcMainContent } from "./MainContent";
import { NdmspcWelcome } from "./Welcome";
import { JobMonitorModal } from "../Base/JobMonitor";
import { NdmspcClusterMainModal } from "../Cluster/NdmspcClusterMainModal";
import { MainToolbar } from "./MainToolbar";
import { NdmspcLogViewer, NdmspcLogViewerModal } from "../Cluster/NdmspcLogViewer";
import { AppMainNav } from "./AppMainNav";
import { PluginConfigurationModal } from "../Plugin/PluginConfiguration";
import { MainConfigurationModal } from "./MainConfiguration";

export const NdmspcAppMain = () => {

  const [ndmspcAnalyses,] = useNdmSpcLocal("ndmspcAnalyses");

  const dashboardBreadcrumb = <Breadcrumb>
    {/* <BreadcrumbItem>Section home</BreadcrumbItem>
      <BreadcrumbItem to="#">Section title</BreadcrumbItem>
      <BreadcrumbItem to="#">Section title</BreadcrumbItem>
      <BreadcrumbItem to="#" isActive>
        Section landing
      </BreadcrumbItem> */}
  </Breadcrumb>;

  const masthead = (
    <Masthead id="light-masthead">
      <MastheadToggle>
        <PageToggleButton variant="plain" aria-label="Global navigation">
          <BarsIcon />
        </PageToggleButton>
      </MastheadToggle>
      <MastheadMain>
        <MastheadBrand href="https://ndmspc.gitlab.io/docs" target="_blank">
          <Brand alt="NDMSPC" heights={{ default: '36px' }} />
        </MastheadBrand>
      </MastheadMain>
      <MastheadContent>
        <MainToolbar />
      </MastheadContent>
    </Masthead>
  )

  const pageNav = <AppMainNav />;

  const sidebar = <PageSidebar>
    <PageSidebarBody>{pageNav}</PageSidebarBody>
  </PageSidebar>;
  const mainContainerId = 'main-content-page-layout-tertiary-nav';
  const pageSkipToContent = <SkipToContent href={`#${mainContainerId}`}>Skip to content</SkipToContent>;

  const services = (
    <>
      <NdmspcClusterMainModal />
      <MainConfigurationModal />
      <PluginConfigurationModal />
      <NdmspcLogViewerModal />
      <JobMonitorModal />
    </>
  )

  return (
    <>
      <Page mainTabIndex={null} header={masthead} sidebar={sidebar} isManagedSidebar skipToContent={pageSkipToContent} mainContainerId={mainContainerId} breadcrumb={dashboardBreadcrumb} isTertiaryNavWidthLimited isBreadcrumbWidthLimited isTertiaryNavGrouped isBreadcrumbGrouped additionalGroupedContent={<PageSection variant={PageSectionVariants.light}>
        {services}
        {(ndmspcAnalyses && Object.keys(ndmspcAnalyses).length > 0 && ndmspcAnalyses?.analysis && ndmspcAnalyses?.analysis.length > 0) ?
          <NdmspcMainContent name="zmq2ws" /> :
          <NdmspcWelcome />
        }
      </PageSection>}>
      </Page>

    </>

  );
}