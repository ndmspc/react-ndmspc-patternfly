import React, { useEffect } from 'react';
import { Nav, NavList } from '@patternfly/react-core';
import { useNdmSpcRedirect } from "@ndmspc/react-ndmspc-core";
import { AnalysisNavGroup } from './Nav/AnalysisNavGroup';
import { PluginNavGroups } from './Nav/PluginNavGroup';
import { ProjectionNavGroup } from './Nav/ProjectionNavGroup';

export const AppMainNav = () => {

  const [, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");

  const onNavSelect = (_event, result) => {
    // console.log(`AppMainNav::onNavSelect ${result.groupId}/${result.itemId}`);
    ndmspcInternalStore.setData({
      type: "nav",
      action: "click",
      payload: {
        group: result.groupId,
        item: result.itemId
      }
    })
  };

  const onNavToggle = (_event, result) => {
    // console.log(`AppMainNav::onNavToggle ${result.groupId} expanded? ${result.isExpanded}`);
    ndmspcInternalStore.setData({
      type: "nav",
      action: "toggle",
      payload: {
        group: result.groupId,
        expanded: result.isExpanded
      }

    })
  };

  return (
    <Nav onSelect={onNavSelect} onToggle={onNavToggle} aria-label="Nav">
      <NavList>
        <AnalysisNavGroup groupId="nav-group-analysis" />
        <ProjectionNavGroup groupId="nav-group-projection" />
        <PluginNavGroups groupId="nav-group-plugin" />
      </NavList>
    </Nav>


  );
};
