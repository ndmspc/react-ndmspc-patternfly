import { useState, useEffect } from 'react'
import { ActionGroup, Button, Form, Modal, ModalVariant } from '@patternfly/react-core'

import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';


export const MainConfiguration = ({ onConfigChange = null }) => {

  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");

  const onSaveClick = () => {
    // const pluginName = ndmspcCurrentPlugin.state.name
    // console.log("Save click: ", pluginName)
    ndmspcAppStore.setData({ ...ndmspcApp, isMainConfigurationOpen: false });
  };
  const onCancelClick = () => {
    // const pluginName = ndmspcCurrentPlugin.state.name
    // console.log("Cancel click: ", pluginName)
    ndmspcAppStore.setData({ ...ndmspcApp, isMainConfigurationOpen: false });
  };

  return (
    <>
      <Form id="main-confiuration">
        <ActionGroup>
          <Button variant="primary" onClick={onSaveClick}>Save</Button>
          <Button variant="secondary" onClick={onCancelClick}>Cancel</Button>
        </ActionGroup>
      </Form>
    </>
  )


}

export const MainConfigurationModal = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [name, setName] = useState(null)
  const [description, setDescription] = useState(null)
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");

  const onConfigChange = (n, d) => {
    if (!n) return
    if (!d) return
    setName(n);
    setDescription(d)
  };

  const handleModalOnClose = () => {
    if (!ndmspcApp) return;
    ndmspcAppStore.setData({ ...ndmspcApp, isMainConfigurationOpen: false });
  };

  useEffect(() => {
    if (!ndmspcApp) return
    setIsModalOpen(ndmspcApp?.isMainConfigurationOpen);
  }, [ndmspcApp]);

  return (
    <Modal position="top" width="80%" title="Main Configuration" variant={ModalVariant.small} description={description || ""} isOpen={isModalOpen} onClose={handleModalOnClose} >
      <MainConfiguration onConfigChange={onConfigChange} />
    </Modal>
  )
}


