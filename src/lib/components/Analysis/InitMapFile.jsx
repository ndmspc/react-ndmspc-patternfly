import React, { useState, useEffect } from 'react';
import { Button, EmptyState, EmptyStateBody, EmptyStateActions, EmptyStateHeader, EmptyStateFooter, EmptyStateIcon, PageSection, PageSectionVariants, ProgressStepper, ProgressStep, EmptyStateVariant } from '@patternfly/react-core';
import CubesIcon from '@patternfly/react-icons/dist/esm/icons/cubes-icon';
import { httpRequest } from "@ndmspc/react-ndmspc-core";
import { useNdmSpcLocal, useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';
import { killJob } from '../Cluster/jobCommands';
export const InitMapFile = ({ commands = [], initDone = null }) => {

  const [buttonName, setButtonName] = useState("Subscribe to cluster")
  const [step, setStep] = useState(0)
  const [processSteps, setProcessSteps] = useState(["pending", "pending", "pending", "pending"])
  const [submitUrl, setSumbitUrl] = useState("")
  const [mgr, setMgr] = useState("")
  const [job, setJob] = useState("")
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws, ndmspcZmq2wsStore] = useNdmSpcLocal("ndmspcZmq2ws");
  const [ndmspcJobs,] = useNdmSpcRedirect("ndmspcJobs");

  const onStart = () => {

    console.log(ndmspcZmq2ws)
    if (buttonName === "Continue") {
      initDone()
      return;
    } else if (buttonName === "Subscribe to cluster") {
      // ndmspcAppStore.setData({ ...ndmspcApp, isZmq2wsOpen: !ndmspcApp.isZmq2wsOpen });
      ndmspcAppStore.setData({ ...ndmspcApp, isZmq2wsOpen: true });
      return
    }

    console.log(ndmspcZmq2ws)
    if (!ndmspcZmq2ws || !ndmspcZmq2ws.connected || !ndmspcZmq2ws.executorUrl || !ndmspcZmq2ws.submitUrl) {
      return
    }

    setStep(1)

    setProcessSteps(["success", "info", "pending", "pending"])
    // const commands = [`sleep 30`];

    if (commands.length ==0) console.error("No commands")

    console.log(commands)

    const jobBody = {
      type: "salsa",
      subtype: "feeder",
      salsa: {
        host: submitUrl,
        job_type: "commands",
      },
      commands: commands,
      command: "sleep",
      args: "10",
      numberOfTasks: 1,
      indexes: null,
      bins: null,
    }
    console.log(ndmspcZmq2ws.sub)
    console.log(ndmspcZmq2ws.submitUrl)
    console.log(ndmspcZmq2ws.executorUrl)
    console.log(jobBody)
    httpRequest(ndmspcZmq2ws.executorUrl, jobBody)
      .then((res) => {
        console.log(res);
        if (res) {
          console.log(res)
          if (res.status === "ok") {
            console.log(`Starting monitoring ${ndmspcZmq2ws.sub.split('|')[1]} ${res.job}`)
            setStep(2)
            setProcessSteps(["success", "success", "info", "pending"])

            setMgr(ndmspcZmq2ws.sub.split('|')[1]);
            setJob(res.job)
          } else {
            setProcessSteps(["success", "success", "danger", "pending"])
            setStep(0)
            setButtonName("Try again")
          }
        }
      })
      .catch((error) => {
        setStep(0)
        setProcessSteps(["success", "danger", "pending", "pending"])
        setButtonName("Try again")
        console.log(error);
      });

  };

  const onCancel = () => {
    console.log(`killing ${mgr} ${job}`)
    killJob(submitUrl, ndmspcZmq2ws.executorUrl, job)
    setStep(0)
    setMgr("")
    setJob("")
    if (ndmspcZmq2ws && ndmspcZmq2ws.connected && ndmspcZmq2ws?.sub) {
      setProcessSteps(["success", "pending", "pending", "pending"])
      setButtonName("Start")
    }
    else {
      setProcessSteps(["danger", "pending", "pending", "pending"])
      setButtonName("Subscribe to cluster")
    }
  }

  useEffect(() => {



    if (ndmspcZmq2ws && ndmspcZmq2ws.connected && ndmspcZmq2ws?.sub) {
      setProcessSteps(["success", "pending", "pending", "pending"])
      setButtonName("Start")
    }
    else {
      setProcessSteps(["danger", "pending", "pending", "pending"])
      setButtonName("Subscribe to cluster")
    }
    setSumbitUrl(ndmspcZmq2ws?.submitUrl)
    

  }, [ndmspcZmq2ws]);

  // useEffect(() => {

  //   console.log(submitUrl)
  // }, [submitUrl]);




  useEffect(() => {
    if (!ndmspcJobs || mgr.length === 0 || job.length === 0) return
    console.log(ndmspcJobs)
    if (ndmspcJobs?.jobs && ndmspcJobs?.mgr === mgr) {
      console.log("Running correct manager ", mgr);
      const currentJob = ndmspcJobs.jobs.find(x => x.name === job);
      console.log(currentJob)
      if (currentJob !== undefined && currentJob?.time?.finished) {
        // setJobStatus(`Done at ${new Date(currentJob?.time?.finished)} D[${currentJob?.rc?.done.length}] F[${currentJob?.rc?.failed.length}]...`)

        if (currentJob?.rc?.failed.length > 0) {
          console.log(`Some jobs are failed !!! D[${currentJob?.rc?.done.length}] F[${currentJob?.rc?.failed.length}]`)
          setStep(0)
          setProcessSteps(["success", "success", "danger", "pending"])
          setButtonName("Try again")
        } else {
          setStep(3)
          setProcessSteps(["success", "success", "success", "success"])
          setButtonName("Continue")

        }
        setMgr("")
        setJob("")
      }

    }
  }, [ndmspcJobs]);




  return (
    <>
      <PageSection variant={PageSectionVariants.light}>
        <EmptyState variant={EmptyStateVariant.sm}>
          <EmptyStateHeader titleText="NDMSPC mapping histogram" headingLevel="h4" />
{/* 
          <EmptyStateBody>
            Start by running job that will create mapping histogram and fill bins with non empty content.
          </EmptyStateBody> */}
          <EmptyStateFooter>
            <ProgressStepper
              isCenterAligned
              aria-label="Progress stepper to create mapping histogram"
            >
              <ProgressStep
                variant={processSteps[0]}
                isCurrent={step == 0}
                description={`Subscribed`}
                id="init-single-job-submition-cluster"
                titleId="init-single-job-submition-cluster"
                aria-label="Cluster subscription step"
              >
                Cluster
              </ProgressStep>
              <ProgressStep
                variant={processSteps[1]}
                isCurrent={step == 1}
                description={`Submiting job to cluster`}
                id="init-single-job-submition-submit"
                titleId="init-single-job-submition-submit"
                aria-label="Job submit step"
              >
                {processSteps[1] === "success" ? "Submited" : "Submitting"}
              </ProgressStep>
              <ProgressStep
                variant={processSteps[2]}
                isCurrent={step == 2}
                description="Creating map projection"
                id="init-single-job-submition-create"
                titleId="init-single-job-submition-cerate"
                aria-label="Create map projection step"
              >
                {processSteps[2] === "success" ? "Done" : "Running"}
              </ProgressStep>
              <ProgressStep
                variant={processSteps[3]}
                isCurrent={step == 3}
                description="Ready to use"
                id="init-single-job-submition-create"
                titleId="init-single-job-submition-cerate"
                aria-label="Ready to use step"
              >
                Finish
              </ProgressStep>
            </ProgressStepper>
            <EmptyStateActions>
              {(step > 0 && step < 3) ? <Button variant="primary" onClick={onCancel}>Cancel</Button> : <Button variant="primary" onClick={onStart}>{buttonName}</Button>}

            </EmptyStateActions>
          </EmptyStateFooter>

        </EmptyState>
      </PageSection >
    </>
  );
};