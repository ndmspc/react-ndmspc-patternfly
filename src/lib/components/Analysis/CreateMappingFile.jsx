import React, { useEffect, useState } from 'react'
import { InputGroup, TextInput, Button, InputGroupItem, FormGroup } from '@patternfly/react-core'
import { NdmspcFileForm } from '../ToDo/FileForm/NdmspcFileForm'
import { NdmspcFileBrowser } from '../ToDo/FileBrowser/NdmspcFileBrowser'
import { InitMapFile } from './InitMapFile'
import { http2root, root2http } from '@ndmspc/react-ndmspc-core'

// https://eos0.ndmspc.io/eos/ndmspc/scratch/out.root
export const CreateMappingFile = ({ onSubmit = null, defaultFileName = 'https://eos.ndmspc.io/eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_pp_AOD/987/phi_leading_3s/AnalysisResults.root', output = "root://eos0.ndmspc.io//eos/ndmspc/scratch/rsn/test/hMap.root" }) => {
  const [fileName, setFileName] = useState(defaultFileName)
  const [objectName, setObjectName] = useState("")
  const [step, setStep] = useState("init")
  const [commands, setCommands] = useState(["ndmspc _macro '/usr/share/ndmspc/macros/NdmspcCreateMap.C' ''"])
  const [fileNameOutput, setFileNameOutput] = useState(defaultFileName)

  const [fileaNameCurrent, setFileaNameCurrent] = useState(null)


  const handleObjectSelect = (obj, treeViewItem, options) => {
    console.log(obj, treeViewItem, options)
    setObjectName(treeViewItem.path)
  }

  const handleFilenameOpen = (filename, isOpen) => {
    setFileName(filename)
    if (isOpen) setFileaNameCurrent(filename)
    else setFileaNameCurrent(null)

  }
  const handleSubmit = () => {
    console.log("Creating analysis maping file: ", fileName, objectName)
    setStep("create")

    setCommands([`ndmspc _macro '/usr/share/ndmspc/macros/NdmspcCreateMap.C' '"${http2root(fileName)[2]}","${objectName}","","${fileNameOutput}"'`])

    // if (!fileName || !fileName.endsWith('.root')) return

  }

  const handleInitDone = () => {

    console.log("InitDone", fileNameOutput)
    const out = root2http(fileNameOutput)[2]
    console.log(out)

    if (fileNameOutput) onSubmit(out, false)
  }

  useEffect(() => {
    if (!output) return
    setFileNameOutput(http2root(output)[2])
  }, [output]);

  if (step === "init") {
    return (
      <>
        <FormGroup label='File Name'>
          <NdmspcFileForm onSubmit={handleFilenameOpen} defaultFileName={fileName} />
        </FormGroup>
        {fileaNameCurrent ?
          <>
            <br />
            <FormGroup label='Select object'>
              <NdmspcFileBrowser fileName={fileaNameCurrent} onObjectSelect={handleObjectSelect} />
            </FormGroup>
            <Button id='CreateMapping file' variant='control' onClick={handleSubmit} isDisabled={objectName.length == 0}>
              Create Map
            </Button>
          </> : null
        }

      </>
    )

  } if (step === "create") {
    return <InitMapFile commands={commands} initDone={handleInitDone} />
  } else {
    return null
  }
}

