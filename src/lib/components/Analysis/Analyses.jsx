import React, { useEffect } from 'react';
import { Label, Card, CardBody, CardHeader, CardTitle, Gallery, Icon } from '@patternfly/react-core';
import PlusCircleIcon from '@patternfly/react-icons/dist/esm/icons/plus-circle-icon';
import CubesIcon from '@patternfly/react-icons/dist/esm/icons/cubes-icon';
import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';
export const NdmspcAnalyses = ({ empty = null, analyses = null, onNewProjection=null }) => {

  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");

  // useEffect(() => {
  //   if (!ndmspcAnalyses) return;
  //   console.log("ndmspcAnalyses : ", ndmspcAnalyses)
  //   // Object.keys(analyses).map((analysis) => {
  //   //   return console.log(`${analysis}`);
  //   // }
  // }, [ndmspcAnalyses]);

  const onClickProjection = (analysisName, projection) => {
    ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, analysis: analysisName, analyses: { ...ndmspcAnalyses.analyses, [analysisName]: { ...ndmspcAnalyses.analyses[analysisName], projection: projection } } });
  }

  const onAddProjection = (analysis) => {
    onNewProjection(analysis);
  }


  return (
    <Gallery hasGutter>
      {empty}
      {analyses && Object.keys(analyses).map((analysis) => {
        return (
          <Card isCompact key={analysis} id={analysis.replace(/ /g, '-')}>
            <CardHeader >
              <Icon>
                <CubesIcon />
              </Icon>
              {/* <img src={icons[analysis.icon]} alt={`${analysis.name} icon`} style={{ maxWidth: '60px' }} /> */}
            </CardHeader>
            <CardTitle>{analysis}</CardTitle>
            <CardBody>
              <Label isCompact key="projection-add" onClick={() => onAddProjection(analysis)}><PlusCircleIcon /></Label>
              {Object.keys(analyses[analysis].projections).map((projection) => {
                return (
                  <Label isCompact key={projection} onClick={() => onClickProjection(analysis, projection)}>{projection} </Label>
                )
              })}
            </CardBody>
          </Card>)
      })}
    </Gallery>
  );
};