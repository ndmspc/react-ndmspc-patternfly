import React, { useEffect, useState } from 'react';
import { useNdmSpcPlugin, useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';
import { NdmspcWorkspaceContent } from './NdmspcWorkspaceContent';
import { getFileObjectNames } from '@ndmspc/react-ndmspc-core';

export const NdmspcWorkspace = ({ histogram = null }) => {
  const [fileName, setFileName] = useState("")
  const [objectNames, setObjectNames] = useState([])
  const [clear, setClear] = useState(false)
  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    setFileName("");
    setObjectNames([]);
    const subscription = ndmspcCurrentPlugin.createExternalSubscription(
      "binObjects",
      (data) => {
        // console.log(data)
        if (!data) return
        setFileName(data.filename);
        if (objectNames.length !== data.names.length || objectNames[0] !== data.names[0])
          setObjectNames(data.names);
        // if (data.names.length === 0)
        //   setClear(true)
        // else {
        //   setClear(false)
        // }
      }
    );
    return () => subscription?.unsubscribe();
  }, [ndmspcCurrentPlugin]);

  // useEffect(() => {
  //   console.log("NdmspcWorkspace:fileName ", fileName)
  // }, [fileName]);


  useEffect(() => {
    if (ndmspcInternal?.type === "job" && ndmspcInternal?.action === "redraw" && ndmspcInternal?.filename) {
      // setFileName(ndmspcInternal?.filename)
      getFileObjectNames(ndmspcInternal?.filename, ndmspcCurrentPlugin)
    }
    if (ndmspcInternal?.type === "job" && ndmspcInternal?.action === "started") {
      // setFileName(ndmspcInternal?.filename)
      setClear(true)
    } else {
      setClear(false)
    }
  }, [ndmspcInternal]);


  return <NdmspcWorkspaceContent histogram={histogram} objectNames={objectNames} fileName={fileName} clear={clear} />

};