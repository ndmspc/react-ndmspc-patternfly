import React, { useEffect, useState, useContext } from 'react';
import { Card, CardBody, CardFooter, CardTitle, Flex, FlexItem, Grid, GridItem, Skeleton, Split, SplitItem, Text } from '@patternfly/react-core';
import { NdmspcCanvas } from '../Canvas/NdmspcCanvas';
import { NdmspcDisplayCanvas } from '../Canvas/DisplayCanvas';
import { AutoSizeComponent } from '../Base/AutoSizeComponent';
import { useNdmSpcLocal, useNdmSpcPlugin, useNdmSpcPlugins } from '@ndmspc/react-ndmspc-core';
import { drawJsrootObjectsFromFile } from '@ndmspc/react-ndmspc-core';
import NdmspcCanvasContext from '../Canvas/NdmspcCanvasContext';




export const NdmspcWorkspaceContent = ({ histogram = null, objectNames = [], fileName = null, clear = false }) => {

  const [layoutName, setLayoutName] = useState("default")
  const [config, setConfig] = useState([])
  const [drawConfig, setDrawConfig] = useState(null)

  const [ndmspcAnalyses] = useNdmSpcLocal("ndmspcAnalyses");
  const ndmspcPlugins = useNdmSpcPlugins()
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  useEffect(() => {
    // if (objectNames.length > 0) return
    // console.log(objectNames)

    let pluginName = ndmspcAnalyses?.plugin ?? "EmptyPlugin"
    const plugin = ndmspcPlugins[pluginName];

    const defaultCanvas = {
      type: "ndmcanvas",
      span: 8,
      rowSpan: 1,
    }

    let drawOpt = ""
    let buildLegend = false
    let filter = []
    if (ndmspcAnalyses?.plugin === "NdmspcBinPlugin") {
      drawOpt = "hist"
      filter = []
      if (objectNames.length >= 4) {
        defaultCanvas.span = 4
        defaultCanvas.rowSpan = 1

      } else {
        defaultCanvas.span = 8
        defaultCanvas.rowSpan = 2
      }


    }
    else if (ndmspcAnalyses?.plugin === "RsnProjectionPlugin") {
      drawOpt = "nostack"
      buildLegend = true
      filter = ["VoightPol2_yield", "VoightPol2_mass", "VoightPol2_width"]
      defaultCanvas.span = 2
      defaultCanvas.rowSpan = 2

    }
    // if (pluginName.length > 0) setName(pluginName)
    if (plugin?.state?.workspaces) {
      const w = [...plugin.state.workspaces[layoutName]]
      let count = 0;
      for (const n in objectNames) {
        // console.log(objectNames[n], filter.includes(objectNames[n]))
        if (filter.length > 0 && !filter.includes(objectNames[n])) continue
        if (++count > 4) continue

        if (clear) {
          w.push({
            ...defaultCanvas,
            type: "ndmempty"
          })
        } else {
          w.push({
            ...defaultCanvas,
            divid: `${objectNames[n]}_id`,
          })
        }

      }
      setConfig(w)

      const drawObjects = []

      let vr = false
      count = 0;
      for (const n in objectNames) {
        if (filter.length > 0 && !filter.includes(objectNames[n])) continue
        if (++count > 4) continue
        if (objectNames.length == 1) {
          vr = true
        } else {
          if (n == 3)
            vr = true
          else
            vr = false
        }
        drawObjects.push({
          divid: `${objectNames[n]}_id`,
          drawOpt,
          buildLegend,
          name: `${objectNames[n]}`,
          vr
        })
      }
      setDrawConfig([{ filename: fileName, objects: drawObjects }])

    }
  }, [ndmspcAnalyses, objectNames, fileName, clear]);


  // useEffect(() => {

  //   if (clear) {
  //     for (const c in config) {
  //       if (config[c].type === "ndmcanvas") {
  //         const list = document.getElementById(config[c].divid);
  //         if (!list) continue
  //         if (list.hasChildNodes()) {
  //           list.removeChild(list.children[0]);
  //           // console.log("Doing clear", clear)
  //           // list.appendChild()
  //         }
  //       }
  //     }
  //   }
  // }, [clear]);

  useEffect(() => {

    console.log(drawConfig)
    drawJsrootObjectsFromFile(drawConfig, ndmspcCurrentPlugin?.state?.isVR)
  }, [drawConfig]);


  // useEffect(() => {
  //   console.log("hhhhh", histogram)
  // }, [histogram]);



  const styleGrid = {
    "borderStyle": "solid",
    "borderWidth": "2px",
    "height": "calc(100vh - 170px)"
    // "height": "80vh"
  }

  const styleGridItem = {
    // "borderStyle": "dashed",
    // "borderWidth": "1px",
    // "height": "80vh"
  }



  const styleGridNdmVr = {
    // "borderStyle": "dashed",
    // "borderWidth": "1px",
    // "height": "250px",
  }


  // useEffect(() => {
  //   console.log("NdmspcWorkspaceContent:",config)
  // }, [config]);


  return <>

    {config && config.length >= 0 ?
      <Grid style={styleGrid}>

        <GridItem key={`${layoutName}-main`} style={styleGridItem} span={4} rowSpan={2}>
          <AutoSizeComponent padding="70" content={
            <NdmspcCanvas histogram={histogram} style={styleGridNdmVr} />} />
        </GridItem>

        {
          config.map((obj, index) => {
            if (obj.type === "ndmvr") {
              return (
                <GridItem key={`${layoutName}-${config.length}-${index}`} style={styleGridItem} span={obj.span || 1} rowSpan={obj.rowSpan || 1}>
                  <AutoSizeComponent padding="70" content={
                    <NdmspcCanvas histogram={histogram} style={styleGridNdmVr} />} />
                </GridItem>

              )
            } else
              if (obj.type === "ndmtabs") {
                return (
                  <GridItem key={`${layoutName}-${config.length}-${index}`} style={styleGridItem} span={obj.span || 1} rowSpan={obj.rowSpan || 1}><AutoSizeComponent content={
                    <NdmspcDisplayCanvas />
                  } /></GridItem>
                )
              }
              else if (obj.type === "ndmcanvas") {
                return (
                  <GridItem key={`${layoutName}-${config.length}-${index}`} style={styleGridItem} span={obj.span || 1} rowSpan={obj.rowSpan || 1}><AutoSizeComponent content={
                    <div id={obj.divid}></div >
                  } /></GridItem>
                )
              }
              else if (obj.type === "ndmempty") {
                return (
                  <GridItem key={`${layoutName}-${config.length}-${index}`} style={styleGridItem} span={obj.span || 1} rowSpan={obj.rowSpan || 1}><AutoSizeComponent content={

                    <Card isPlain>
                      <CardBody>
                        <Skeleton width="20%" />
                        <br />
                        <Skeleton width="40%" />
                        <br />
                        <Skeleton width="50%" />
                        <br />
                        <Skeleton width="50%" />
                        <br />
                        <Skeleton width="50%" />
                        <br />
                        <Skeleton width="50%" />
                        <br />
                        <Skeleton width="50%" />
                        <br />
                        <Skeleton width="30%" />
                        <br />
                      </CardBody>
                    </Card>

                  } /></GridItem >
                )
              }


          })
        }
      </Grid>
      : (<Text>Emtpy workspace</Text>)
    }

  </>;


  // return <>

  //   <Grid style={styleGrid}>
  //     <GridItem style={styleGridItem} span={4} rowSpan={1}>
  //       <AutoSizeComponent padding="70" content={
  //         <NdmspcCanvas histogram={histogram.histogram} style={styleGridNdmVr} />} />
  //     </GridItem>
  //     <GridItem style={styleGridItem} span={4} rowSpan={1}>
  //       <AutoSizeComponent content={
  //         <div id="current_proj"></div >
  //       } />
  //     </GridItem>
  //     <GridItem style={styleGridItem} span={4}><AutoSizeComponent content={
  //       <NdmspcDisplayCanvas />
  //     } /></GridItem>

  //     <GridItem style={styleGridItem} span={3} rowSpan={1}>
  //       <AutoSizeComponent content={
  //         <div id="sigBg_id"></div >
  //       } />
  //     </GridItem>
  //     <GridItem style={styleGridItem} span={3} rowSpan={1}>
  //       <AutoSizeComponent content={
  //         <div id="bg_id"></div >
  //       } />
  //     </GridItem>
  //     <GridItem style={styleGridItem} span={3} rowSpan={1}>
  //       <AutoSizeComponent content={
  //         <div id="bgNorm_id"></div >
  //       } />
  //     </GridItem>
  //     <GridItem style={styleGridItem} span={3} rowSpan={1}>
  //       <AutoSizeComponent content={
  //         <div id="peak_id"></div >
  //       } />
  //     </GridItem>

  //     {/* <GridItem style={styleGridItem} span={2} rowSpan={1}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={2}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={4}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={2}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={2}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={2}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={4}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={2}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={4}><AutoSizeComponent /></GridItem> */}
  //     {/* <GridItem style={styleGridItem} span={4}><AutoSizeComponent /></GridItem> */}
  //   </Grid>

  // </>;

};