import React from "react";
import "@fontsource/red-hat-display"; // Defaults to weight 400.
import "@fontsource/red-hat-text"; // Defaults to weight 400.
import './NdmspcApp.css'
import {
  redirectStore,
  localStore,
  NdmSpcContext,
  NdmVrContext,
  WebSocketStreamBroker,
  NdmspcInternal
} from "@ndmspc/react-ndmspc-core";
import { NdmspcAppMain } from "./Main/AppMain";

export const NdmspcApp = ({ plugins = null }) =>

  // <NdmVrContext.Provider
  //   value={{
  //     ndmvrCameraPosRot: localStore('ndmvrCameraPosRot'),
  //     ndmvrInputDevice: redirectStore('ndmvrInputDevice'),
  //     ndmvrSchema: redirectStore('ndmvrSchema'),
  //     ndmvrFullscreen: redirectStore('ndmvrFullscreen')
  //   }}>
    <NdmSpcContext.Provider
      value={{
        ndmspcPlugins: plugins,
        zmq2ws: WebSocketStreamBroker(),
        ndmvr: WebSocketStreamBroker(),
        ndmspcApp: localStore("ndmspcApp"),
        ndmspcZmq2ws: localStore("ndmspcZmq2ws"),
        ndmspcAnalyses: localStore("ndmspcAnalyses"),
        ndmspcConfiguration: redirectStore("ndmspcConfiguration"),
        ndmspcInternal: redirectStore("ndmspcInternal"),
        ndmspcClusterSubscribe: redirectStore("ndmspcClusterSubscribe"),
        ndmspcNode: redirectStore("ndmspcNode"),
        ndmspcJobs: redirectStore("ndmspcJobs"),
        // ndmspcConfig : config
      }}
    >
      <NdmspcInternal />
      <NdmspcAppMain />
    </NdmSpcContext.Provider>
  // </NdmVrContext.Provider>

