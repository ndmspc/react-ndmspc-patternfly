import React, { useContext, useMemo, useState } from "react";
import { NdmVr } from "@ndmspc/ndmvr";
import { Bullseye } from "@patternfly/react-core";
import NdmspcCanvasContext from "./NdmspcCanvasContext";

// const views = []

// const themes = [
//   'def',
//   'fire',
//   'nebula',
//   'rainforest',
//   'nature',
//   'water',
//   'sunflower',
//   'blood',
//   'magma',
//   'jupiter',
//   'magic'
// ]

export const NdmvrCanvas = ({ style }) => {
  const [range, setRange] = useState(6);
  const [theme, setTheme] = useState("def");
  const { id, histogram, projNames, selectedBins, bannerIds, app, analysis, zmq2ws, isVR, plugin } =
    useContext(NdmspcCanvasContext).data;
  const {
    onBinClick,
    onBinDbClick,
    onProgress,
    onBinHover,
    onViewChange,
    onExecuteCommand,
  } = useContext(NdmspcCanvasContext).functions;
  const vrBannerIds = useMemo(() => {
    if (bannerIds) {
      return bannerIds.slice(0, 4).map((x) => "vr_" + x);
    } else {
      return [
        "vr_banner_1",
        "vr_banner_2",
        "vr_banner_3",
        "vr_banner_4",
        "vr_banner_5",
      ];
    }
  }, [bannerIds]);

  // extending the function
  const extendedOnBinClick = (data) => {
    // console.log("Cliiiiiick");
    if (!onBinClick) return;
    data.bannerIds = vrBannerIds;
    data.projNames = projNames;
    data.histogramId = id;
    data.app = app;
    data.zmq2ws = zmq2ws;
    data.analysis = analysis;
    data.isVR = isVR
    onBinClick(data);
  };

  const extendedOnBinDbClick = (data) => {
    if (!onBinDbClick) return;
    data.bannerIds = vrBannerIds;
    data.projNames = projNames;
    data.histogramId = id;
    data.app = app;
    data.zmq2ws = zmq2ws;
    data.analysis = analysis;
    data.isVR = isVR
    onBinDbClick(data);
  };

  const extendedOnBinHover = (data) => {
    if (!onBinHover) return;
    if (data) {
      data.bannerIds = vrBannerIds;
      data.projNames = projNames;
      data.histogramId = id;
      data.app = app;
      data.zmq2ws = zmq2ws;
      data.analysis = analysis;
      data.isVR = isVR
      onBinHover(data);
    }
  };


  return histogram ? (
    <NdmVr
      data={{
        histogram: histogram,
        id: id,
        projectionsNames: projNames,
        projPanelIds: vrBannerIds,
        background: {
          radius: "3000",
          height: "2048",
        },
      }}
      selectedBins={selectedBins}
      onClick={extendedOnBinClick}
      onDbClick={extendedOnBinDbClick}
      onHover={extendedOnBinHover}
      onView={onViewChange}
      onProgress={onProgress}
      onExeCommand={onExecuteCommand}
      experimental={true}
      style={style}
    />
  )
    : (
      <Bullseye>
        <div>
          Displaying of this type of object is not supported !!!
        </div>
      </Bullseye>
    );

};
