import React, { useState, useEffect } from "react";
import { Flex, FlexItem, Spinner, Switch } from "@patternfly/react-core";
import { NdmvrCanvas } from "./NdmvrCanvas";
import { JsrootCanvas } from "./JsrootCanvas";
import NdmspcCanvasContext from "./NdmspcCanvasContext";
import { useNdmSpcLocal, useNdmSpcPlugin, useNdmSpcPlugins } from "@ndmspc/react-ndmspc-core";
// import { ndmspcCurrentPlugin } from "../../../AppGlobalContext.js";

export const NdmspcCanvas = ({
  id = "ndmspc-canvas",
  histogram,
  projNames = [],
  // selectedBins = [],
  bannerIds = ["proj", "proj1", "proj2", "proj3", "proj4"],
  // onBinClick = null,
  // onBinDbClick = null,
  // onBinHover = null,
  // onViewChange = null,
  // onProgress = null,
  // onExecuteCommand = null,
  // onContextOption = null,
  style,
  height = "50vh",
  width = "100%"
}) => {
  const [vr, setVr] = useState(null);
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws, ndmspcZmq2wsStore] = useNdmSpcLocal("ndmspcZmq2ws");
  const [ndmspcAnalyses,] = useNdmSpcLocal("ndmspcAnalyses");
  
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  const [selectedBins, setSelectedBins] = useState([])
  // const [selectedBins, setSelectedBins] = useState(
  // ndmspcCurrentPlugin?.state?.selectedBins ? Array.from(ndmspcCurrentPlugin.state?.selectedBins?.keys()) : []
  // );
  const [enterPressed, setEnterPressed] = useState(0);

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    setVr(ndmspcCurrentPlugin.state.isVR)

    const subscription = ndmspcCurrentPlugin.createExternalSubscription(
      "binSelect",
      (data) => {
        setSelectedBins(data);
      },
      null
    );
    return () => subscription?.unsubscribe();
  }, [ndmspcCurrentPlugin]);

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    const subscription = ndmspcCurrentPlugin.createExternalSubscription(
      "onEnterPressed",
      (data) => {
        setEnterPressed(data);
      },
      null
    );
    return () => subscription?.unsubscribe();
  }, [ndmspcCurrentPlugin]);


  // useEffect(() => {
  //   console.log(histogram)
  // }, [histogram]);

  // useEffect(() => {
  //   console.log(ndmspcAnalyses)
  // }, [ndmspcAnalyses]);

  // useEffect(() => {
  //   console.log(selectedBins);
  //   console.log(ndmspcCurrentPlugin.state.selectedBins)
  // }, [selectedBins]);

  useEffect(() => {
    if (ndmspcCurrentPlugin?.state?.selectedBins) {
      ndmspcCurrentPlugin.state.selectedBins.clear();
      // setSelectedBins(Array.from(ndmspcCurrentPlugin.state.selectedBins.keys()));
    } else {
      setSelectedBins([]);
    }
  }, [enterPressed]);




  const onVrChange = () => {
    const svr = !vr
    setVr(svr);
    // ndmspcAppStore.setData({ ...ndmspcApp, isVR: svr });
    ndmspcCurrentPlugin.state.isVR = svr
  };

  // useEffect(() => {
  //   // if (ndmspcApp?.isVR !== vr)
  //   console.log(ndmspcApp?.isVR)
  //   console.log(ndmspcCurrentPlugin.state.isVR)
  //   if (ndmspcApp?.isVR && ndmspcApp?.isVR !== vr) {
  //     setVr(ndmspcApp?.isVR);
  //   }

  //   if (ndmspcApp?.isVR && ndmspcApp?.isVR !== ndmspcCurrentPlugin.state.isVR) {
  //     ndmspcCurrentPlugin.state.isVR = ndmspcApp?.isVR
  //   }


  // }, [ndmspcApp]);

  // useEffect(() => {
  //   console.log(selectedBins)
  // }, [selectedBins]);


  const settingsBarStyle = {
    // marginTop: 8,
    // padding: 14,
    // background: "#e6e7e8",
    // minHeight: { height }
    // height: { height },
    // width: { width },
  };

  return ndmspcApp && vr !== null ? (
    <NdmspcCanvasContext.Provider
      value={{
        data: {
          id,
          histogram,
          projNames,
          bannerIds,
          selectedBins,
          isVR: vr,
          app: ndmspcApp,
          analysis: ndmspcAnalyses ? ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis] : null,
          zmq2ws: ndmspcZmq2ws,
          plugin: ndmspcCurrentPlugin
        },
        functions: {
          onBinClick: ndmspcCurrentPlugin?.userFunctions?.onBinClick,
          onBinDbClick: ndmspcCurrentPlugin?.userFunctions?.onBinDbClick,
          onBinHover: ndmspcCurrentPlugin?.userFunctions?.onBinHover,
          onProgress: ndmspcCurrentPlugin?.userFunctions?.onProgress,
          onViewChange: ndmspcCurrentPlugin?.userFunctions?.onViewChange,
          onExecuteCommand: ndmspcCurrentPlugin?.userFunctions?.onExecuteCommand,
          onContextOption: ndmspcCurrentPlugin?.userFunctions?.onContextOption,
        }
      }}
    >
      <Flex direction={{ default: "column" }} style={settingsBarStyle}>
        <FlexItem>
          <Switch
            id="simple-switch"
            label="VR on"
            labelOff="VR off"
            isChecked={vr}
            onChange={onVrChange}
          />
        </FlexItem>
        <FlexItem>
          {vr ? <NdmvrCanvas style={style} /> : <JsrootCanvas style={style} />}
        </FlexItem>
      </Flex>
    </NdmspcCanvasContext.Provider>
  ) : <Spinner />;

};
