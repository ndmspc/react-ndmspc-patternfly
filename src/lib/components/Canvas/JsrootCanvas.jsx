import React, { useState, useLayoutEffect, useContext } from "react";
import { draw, redraw } from "@ndmspc/react-ndmspc-core";
import NdmspcCanvasContext from "./NdmspcCanvasContext";

// let isDrawing = false;


export const JsrootCanvas = ({ style }) => {
  const {
    id,
    histogram,
    projNames,
    selectedBins, // TODO find right implementation
    bannerIds,
    isVR,
    app, analysis, plugin, zmq2ws
  } = useContext(NdmspcCanvasContext).data;
  const { onBinClick, onBinDbClick, onBinHover, onContextOption } =
    useContext(NdmspcCanvasContext).functions;
  // const [histogramLocal, setHistogramLocal] = useState(histogram);
  const [histogramPainter, setHistogramPainter] = useState(null);

  // const [isDrawing, setIsDrawing] = useState(true);

  useLayoutEffect(() => {
    drawHisto();
  }, [histogram, plugin]);

  // useLayoutEffect(() => {
  //   // appendContextMenu();
  // }, [histogramPainter, projNames]);

  // extending the function
  const extendedOnBinClick = (data) => {
    if (!onBinClick) return;
    data.bannerIds = bannerIds;
    data.projNames = projNames;
    data.histogramId = id;
    data.app = app;
    data.zmq2ws =zmq2ws;
    data.analysis = analysis;
    data.isVR = isVR
    onBinClick(data);
  };

  const extendedOnBinDbClick = (data) => {
    if (!onBinDbClick) return;
    data.bannerIds = bannerIds;
    data.projNames = projNames;
    data.histogramId = id;
    data.app = app;
    data.zmq2ws =zmq2ws;
    data.analysis = analysis;
    data.isVR = isVR
    onBinDbClick(data);
  };

  const extendedOnBinHover = (data) => {
    if (!onBinHover) return;
    if (data) {
      data.bannerIds = bannerIds;
      data.projNames = projNames;
      data.histogramId = id;
      data.app = app;
      data.zmq2ws =zmq2ws;
      data.analysis = analysis;
      data.isVR = isVR
      onBinHover(data);
    }
  };

  const drawHisto = async () => {
    if (histogram === null) {
      setHistogramPainter(null);
      return null
    }
    let opt;
    if (histogram._typename.startsWith("TH2")) opt = "colz";
    if (histogram._typename.startsWith("TH3")) opt = "box2";
    const histodiv = document.getElementById('jsroot_histo');
    let painter = histodiv.childNodes.length === 0 ? await draw("jsroot_histo", histogram, opt) : await redraw("jsroot_histo", histogram, opt);
    // let painter = await draw("jsroot_histo", histogram, opt);
    if (painter === null) {
      setHistogramPainter(null);
      return null;
    }
    painter.configureUserClickHandler(extendedOnBinClick);
    painter.configureUserTooltipHandler(extendedOnBinHover, 200);
    painter.configureUserDblclickHandler(extendedOnBinDbClick);
    setHistogramPainter(painter);

  };

  const appendContextMenu = async () => {
    if (histogramPainter && projNames.length) {
      histogramPainter.oldFillHistContextMenu =
        histogramPainter.fillHistContextMenu;

      histogramPainter.fillHistContextMenu = function (menu) {
        const tip = menu.painter.getToolTip(menu.getEventPosition());

        menu.add(`sub:Histogram bin [${tip.binx}, ${tip.biny}]`);
        projNames.forEach((child) => {
          menu.add(`Projection ${child}`, () => {
            onContextOption({
              data: projNames,
              selected: child,
            });
          });
        });
        menu.add("endsub:");
        return this.oldFillHistContextMenu(menu);
      };
    }
  };

  return <div id="jsroot_histo" style={style} />;
  // return <div id="jsroot_histo" />;

};

