import React, { useEffect, useState } from "react";
import { Flex, FlexItem, Tab, Tabs, TabTitleText } from "@patternfly/react-core";
import {
  displayImageOfProjection,
  openFile,
  redraw,
} from "@ndmspc/react-ndmspc-core";
import { useNdmSpcLocal, useNdmSpcPlugin, useNdmSpcPlugins } from "@ndmspc/react-ndmspc-core";
import { drawJsrootObjectsFromFile } from "@ndmspc/react-ndmspc-core";

const projectionStyle = {
  // display: "block",
  // width: `700px`,
  // height: `575px`,
  width: `300px`,
  height: `300px`,
  // border: "2px solid grey",
};


export const NdmspcDisplayCanvas = ({ style }) => {

  const [activeTabKey, setActiveTabKey] = useState(0);
  const [projections, setProjections] = useState(null);
  const [vr, setVr] = useState(false);
  const [ndmspcApp,] = useNdmSpcLocal("ndmspcApp");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()


  useEffect(() => {
    if (ndmspcApp?.isVR !== vr)
      setVr(ndmspcApp?.isVR);
  }, [ndmspcApp]);

  const handleTabClick = (event, tabIndex) => {
    setActiveTabKey(tabIndex);
  };

  const handleProjectionNames = (data) => {
    setProjections(data);
  };

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    const subscription = ndmspcCurrentPlugin.createExternalSubscription(
      "binObjects",
      handleProjectionNames,
      null
    );
    return () => subscription?.unsubscribe();
  }, [ndmspcCurrentPlugin]);

  useEffect(() => {
    // readProjectionAndPatchOnVrPanel(vr);
    // console.log(drawConfig)

    // drawJsrootObjectsFromFile(drawConfig)


    
  }, [activeTabKey, projections]);

  const readProjectionAndPatchOnVrPanel = async (vr) => {
    if (projections?.names?.length) {
      const currProj = projections.names[activeTabKey];
      const displayProjs = [currProj + "_id"];
      // const displayProjs = [];
      
      displayProjs.push("current_proj");
      await readProjectionFromFile(projections.filename, currProj, displayProjs);
      if (vr) {
        await displayImageOfProjection(
          // currProj + "_id",
          "current_proj",
          ["vr_proj"],
          "800px",
          "800px"
        );
      }
    }
  };

  // TODO add to plugins
  const readProjectionFromFile = async (filename, projName, targetElmIds) => {
    // console.log(filename,projName, targetElmIds)
    const rootFile = await openFile(filename).catch(console.log);
    // console.log(filename,projName)
    if (!rootFile) return
    const proj = await rootFile.readObject(projName);
    if (proj) {
      const painters = new Map();
      for (const targetElmId of targetElmIds) {
        const hPainter = await redraw(targetElmId, proj, "colz");
        painters.set(targetElmId, hPainter);
      }
    }
  };


  // return <div id="jsroot_histo" style={style} />;

  return (
    <>
      {projections?.names?.length ? (
        <Flex direction={{ default: 'column' }}>
          <FlexItem>
            {/* <div id={projections.names[activeTabKey] + "_id"} style={style} /> */}
            <Tabs activeKey={activeTabKey} onSelect={handleTabClick}>
              {projections.names ? projections.names.map((x, i) => (
                <Tab
                  key={x + i}
                  eventKey={i}
                  title={<TabTitleText key={`${i}_header`}>{x}</TabTitleText>}
                />
              )) : null}
            </Tabs>


          </FlexItem>
        </Flex >
      ) : null
        // <div id="jsroot_histo" style={style} />
      }
    </>
  );
};


