import { useState, useEffect } from 'react'
import { ActionGroup, Button, Form, Modal, ModalVariant } from '@patternfly/react-core'

import { JsonEditor as Editor } from "jsoneditor-react";
import "jsoneditor-react/es/editor.min.css";

import { useNdmSpcLocal, useNdmSpcPlugin, useNdmSpcRedirect, http2root } from '@ndmspc/react-ndmspc-core';
import { PluginConfigLoad } from './PluginConfigLoad';
import { applyConfigFromAnalysis } from '../../utils/core';

export const PluginConfiguration = ({ onConfigChange = null }) => {

  const [config, setConfig] = useState(null)
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcConfiguration, ndmspcConfigurationStore] = useNdmSpcRedirect("ndmspcConfiguration");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  const handleConfigModified = (c) => {
    if (!c) return
    setConfig(c)
  };

  const onConfigLoad = (cfg, name, desc) => {
    setConfig(cfg)
    if (onConfigChange) onConfigChange(name, desc)
  };

  const onClickConfigLoad = (_event, value) => {
    setConfig(null)
    if (onConfigChange) onConfigChange(null, null)
  };


  const applyNewConfiguration = () => {
    const pluginName = ndmspcCurrentPlugin.state.name
    console.log("Applying configuration : ", pluginName)
    if (!config) return
    const analysis = ndmspcAnalyses.analyses[ndmspcAnalyses.analysis]
    applyConfigFromAnalysis(config, analysis, pluginName, ndmspcConfiguration, ndmspcConfigurationStore)
    ndmspcAppStore.setData({ ...ndmspcApp, isPluginConfigurationOpen: false });
  };

  const onSaveClick = () => {
    applyNewConfiguration()
    // const pluginName = ndmspcCurrentPlugin.state.name
    // console.log("Save click: ", pluginName)
    ndmspcAppStore.setData({ ...ndmspcApp, isPluginConfigurationOpen: false });
  };
  const onCancelClick = () => {
    // const pluginName = ndmspcCurrentPlugin.state.name
    // console.log("Cancel click: ", pluginName)
    ndmspcAppStore.setData({ ...ndmspcApp, isPluginConfigurationOpen: false });
  };


  useEffect(() => {
    if (!ndmspcConfiguration) return
    if (!ndmspcCurrentPlugin) return
    // if (!config) return
    // console.log("new configuration", ndmspcConfiguration)
    const pluginName = ndmspcCurrentPlugin.state.name
    if (!pluginName) return
    if (!ndmspcConfiguration[pluginName]) return
    const currentPluginConfigurationName = ndmspcConfiguration[pluginName].current
    setConfig(ndmspcConfiguration[pluginName][currentPluginConfigurationName].config)

  }, [ndmspcConfiguration, ndmspcCurrentPlugin]);

  return config ? (
    <>
      <Form id="plugin-confiuration">
        <ActionGroup>
          <Button variant="tertiary" onClick={onClickConfigLoad}>Import config</Button>
        </ActionGroup>
        <Editor value={config} onChange={handleConfigModified} />
        {/* <FormObject name="main" content={properties} type={isSimple ? "expandable" : "accordion"} /> */}
        <ActionGroup>
          <Button variant="primary" onClick={onSaveClick}>Save</Button>
          <Button variant="secondary" onClick={onCancelClick}>Cancel</Button>
        </ActionGroup>
      </Form>
    </>
  )
    : <PluginConfigLoad onConfigLoad={onConfigLoad} />

}

export const PluginConfigurationModal = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [name, setName] = useState(null)
  const [description, setDescription] = useState(null)
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");

  const onConfigChange = (n, d) => {
    if (!n) return
    if (!d) return
    setName(n);
    setDescription(d)
  };

  const handleModalOnClose = () => {
    if (!ndmspcApp) return;
    ndmspcAppStore.setData({ ...ndmspcApp, isPluginConfigurationOpen: false });
  };

  useEffect(() => {
    if (!ndmspcApp) return
    setIsModalOpen(ndmspcApp?.isPluginConfigurationOpen);
  }, [ndmspcApp]);

  return (
    <Modal position="top" width="80%" title={name || "No configuration"} variant={ModalVariant.small} description={description || "Please load configuration by uploading config file."} isOpen={isModalOpen} onClose={handleModalOnClose} >
      {/* <PluginConfiguration name={schema?.name} descripion={schema?.schema.openAPIV3Schema.description} properties={schema?.schema.openAPIV3Schema.properties} onSchemaReset={onSchemaReset} /> */}
      <PluginConfiguration onConfigChange={onConfigChange} />
    </Modal>
  )
}


