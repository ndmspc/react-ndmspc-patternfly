import React from 'react';
import { Button, FileUpload } from '@patternfly/react-core';
import { useNdmSpcLocal, useNdmSpcRedirect } from '@ndmspc/react-ndmspc-core';
import { applyConfigFromAnalysis } from '../../utils/core';


export const PluginConfigLoad = ({  onConfigLoad = null }) => {
  const [value, setValue] = React.useState('');
  const [filename, setFilename] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcConfiguration, ndmspcConfigurationStore] = useNdmSpcRedirect("ndmspcConfiguration");

  const handleFileInputChange = (_, file) => {
    setFilename(file.name);
  };
  const handleTextChange = (_event, value) => {
    setValue(value);
  };
  const handleDataChange = (_event, value) => {
    setValue(value);
  };
  const handleClear = _event => {
    setFilename('');
    setValue('');
  };
  const handleFileReadStarted = (_event, _fileHandle) => {
    setIsLoading(true);
  };
  const handleFileReadFinished = (_event, _fileHandle) => {
    setIsLoading(false);
  };
  const handleApply = () => {
    let cfg = JSON.parse(value);

    // const analysis = ndmspcAnalyses.analyses[ndmspcAnalyses.analysis]
    // // console.log(analysis)
    // const projection = analysis.projections[analysis.projection]
    // // console.log(projection)

    // applyConfigFromAnalysis(cfg, analysis, ndmspcConfiguration, ndmspcConfigurationStore)

    const name = Object.keys(cfg)[0] || "no_config"
    onConfigLoad(cfg, name, `Configuration of '${name}'`)
  };

  return (<>
    <FileUpload id="text-file-simple" type="text" value={value} filename={filename} filenamePlaceholder="Drag and drop a file or upload one" onFileInputChange={handleFileInputChange} onDataChange={handleDataChange} onTextChange={handleTextChange} onReadStarted={handleFileReadStarted} onReadFinished={handleFileReadFinished} onClearClick={handleClear} isLoading={isLoading} allowEditingUploadedText={false} browseButtonText="Upload" />
    <Button variant="primary" onClick={handleApply} isAriaDisabled={value.length == 0}>Apply</Button>
  </>)
};