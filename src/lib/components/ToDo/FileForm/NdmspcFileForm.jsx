import React, { useEffect, useState } from 'react'
import { InputGroup, TextInput, Button, InputGroupItem } from '@patternfly/react-core'

// https://eos0.ndmspc.io/eos/ndmspc/scratch/out.root
export const NdmspcFileForm = ({ onSubmit = null, defaultFileName = 'https://eos0.ndmspc.io/eos/ndmspc/scratch/ndmspc/export/test/ndmspc.root', disabled = false }) => {
  const [fileName, setFileName] = useState(defaultFileName)
  const [isOpen, setIsOpen] = useState(false)


  useEffect(() => {
    console.log(fileName, isOpen)

  }, [fileName]);


  const handleSubmit = () => {
    if (!fileName || !fileName.endsWith('.root')) return
    if (onSubmit) {
      onSubmit(fileName, !isOpen)
    }
    setIsOpen(!isOpen)
  }
  return (
    <InputGroup label="Full name">
      <InputGroupItem isFill >
        <TextInput
          id='fileNameInput'
          aria-label='Input filename'
          value={fileName}
          onChange={(_event, value) => setFileName(value)}
          isDisabled={isOpen || disabled}
        />
      </InputGroupItem>
      <InputGroupItem>
        <Button id='fileOpen' variant='control' onClick={handleSubmit}>
          {isOpen ? "Close" : "Open"}
        </Button>
      </InputGroupItem>
    </InputGroup>
  )
}

