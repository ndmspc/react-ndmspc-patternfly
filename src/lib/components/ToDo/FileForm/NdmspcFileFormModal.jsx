import React, { useState, useEffect } from 'react'
import { Modal } from '@patternfly/react-core'
import { NdmspcFileForm } from './NdmspcFileForm';

// https://eos0.ndmspc.io/eos/ndmspc/scratch/out.root
export const NdmspcFileFormModal = ({ onSubmit = null, isOpen = false, defaultFileName = 'https://eos0.ndmspc.io/eos/ndmspc/scratch/ndmspc/export/test/ndmspc.root' }) => {
  const [isModalOpen, setIsModalOpen] = useState(isOpen);
  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  const handleModalSubmit = (filename) => {
    setIsModalOpen(false);
    onSubmit(filename)
  };

  useEffect(() => {
    setIsModalOpen(isOpen);
  }, [isOpen]);

  return (
    <Modal width="50%" title="Input filename" isOpen={isModalOpen} onClose={handleModalToggle} >
      <NdmspcFileForm onSubmit={handleModalSubmit} defaultFileName={defaultFileName} />
    </Modal>
  )
}
