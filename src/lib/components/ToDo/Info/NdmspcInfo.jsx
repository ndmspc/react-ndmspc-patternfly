import { React, useLayoutEffect, useState } from "react";
import { useStreamBrokerIn, useStreamBrokerOut } from "@ndmspc/react-ndmspc-core";
import { Spinner } from "@patternfly/react-core";
// import { DisplayCanvas } from "../Canvas/DisplayCanvas";

export const NdmspcInfo = ({ name }) => {
  const _ws = useStreamBrokerOut("_ws", name);
  const zmq = useStreamBrokerIn("zmq", name);
  const [zmqData, setZmqData] = useState(null);

  useLayoutEffect(() => {
    setZmqData(zmq);
  }, [zmq]);

  useLayoutEffect(() => {
    if (Object.keys(_ws).length) {
      // console.log(_ws);
      if (_ws.type === "_ws") {
        if (_ws.action === "state") {
          if (!_ws.payload.connected) setZmqData(null);
        }
      }
    }
  }, [_ws]);

  return (
    <>
      {zmqData?.data ? (
        <>
          <p>name: {zmqData.data.name}</p>
          <p>sub: {zmqData.data.sub}</p>
          <p>Workers: {zmqData.data.data.node?.slots}</p>
          <p>Submit url: {zmqData.data.data.node?.submitUrl}</p>
        </>
      ) : (
        <Spinner size="lg" aria-label="Loading" />
      )}
      {/* <DisplayCanvas /> */}
    </>
  );
};

