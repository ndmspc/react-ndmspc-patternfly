

import React, { useEffect, useState } from "react";
import { Spinner, TreeView } from "@patternfly/react-core";
import FolderIcon from "@patternfly/react-icons/dist/esm/icons/folder-icon";
import FolderOpenIcon from "@patternfly/react-icons/dist/esm/icons/folder-open-icon";
import { ApplicationsIcon } from "@patternfly/react-icons";
import { openFile } from "@ndmspc/react-ndmspc-core";

export const NdmspcFileBrowser = ({ fileName, onObjectSelect = null }) => {
  const [activeItems, setActiveItems] = useState({});
  const [state, setState] = useState(false);
  const [options, setOptions] = useState(null);

  const [path, setPath] = useState(null)

  const getChildren = async (rootFile) => {
    console.log(rootFile)
    const children = [];
    for (let key of rootFile.fKeys) {
      let obj = await rootFile.readObject(key.fName);
      // console.log(obj)
      if (obj._typename.startsWith("TDirectory")) {
        const child = {
          name: obj.dir_name.split("/").pop(),
          defaultExpanded: true,
          customBadgeContent: { childObj: obj },
        };
        children.push(child);

      } else if (obj._typename.startsWith("TH")) {
        const child = {
          name: obj.fName,
          id: obj.fName,
          path: `${rootFile.dir_name}/${obj.fName}`,
          defaultExpanded: false,
          customBadgeContent: { childObj: obj },
          icon: <ApplicationsIcon />,
        };
        children.push(child);
      } else if (obj._typename.startsWith("TF")) {
        const child = {
          name: obj.fName,
          id: obj.fName,
          path: `${rootFile.dir_name}/${obj.fName}`,
          defaultExpanded: false,
          customBadgeContent: { childObj: obj },
          icon: <ApplicationsIcon />,
        };
        children.push(child);
      }
    }
    return children;
  }

  useEffect(() => {
    if (!fileName) return;
    // console.log("fileName")
    const readProjectionFromFile = async (fileName) => {
      const rootFile = await openFile(fileName);
      // console.log("Rereading file " + rootFile);
      const options = await getChildren(rootFile)
      setOptions(options);
    };
    // console.log("Filename changed in NdmspcFileBrowser : " + fileName)

    readProjectionFromFile(fileName).catch((e) => console.log(e));
  }, [fileName]);

  const onSelect = async (evt, treeViewItem) => {
    // console.log(treeViewItem);
    const file = treeViewItem.customBadgeContent.childObj;
    // console.log(file);

    if (file._typename === "TDirectory") {
      treeViewItem.children = await getChildren(file);
    } else {
      treeViewItem.children = null;
      // console.log(activeItems)
      onObjectSelect(file, treeViewItem, options);
    }

    // console.log(treeViewItem.children);
    if (treeViewItem && !treeViewItem.children) {
      setActiveItems([treeViewItem.children]);
    }
    setState(!state)

  };

  return (
    <>
      {options ? (
        <TreeView
          data={options}
          activeItems={activeItems}
          onSelect={onSelect}
          icon={<FolderIcon />}
          expandedIcon={<FolderOpenIcon />}
        />
      )
        : (
          <Spinner />
        )
      }
    </>
  );
};

