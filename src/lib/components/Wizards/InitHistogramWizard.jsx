import React, { useState, useEffect } from 'react';
import { DataList, DataListCell, DataListItem, DataListItemCells, DataListItemRow, Text, DualListSelector, FormGroup, InputGroup, InputGroupItem, Icon, TextInput, TextVariants, Wizard, WizardStep, TextContent, HelperTextItem } from '@patternfly/react-core';
import CheckCircleIcon from '@patternfly/react-icons/dist/esm/icons/check-circle-icon';
import ExclamationTriangleIcon from '@patternfly/react-icons/dist/esm/icons/exclamation-triangle-icon';


import { useNdmSpcLocal } from '@ndmspc/react-ndmspc-core';
import { NdmspcFileForm } from '../ToDo/FileForm/NdmspcFileForm';
import { NdmspcFileBrowser } from '../ToDo/FileBrowser/NdmspcFileBrowser';
import { http2root, isObjectEmpty, root2http, urlExists } from '@ndmspc/react-ndmspc-core';
import { NdmspcClusterMain } from '../Cluster/NdmspcClusterMainModal';
import { CreateMappingFile } from '../Analysis/CreateMappingFile';

export const InitHistogramWizard = ({ onClose = null, defaultAnalysisName = 'myAnalysis'}) => {

  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws, ndmspcZmq2wsStore] = useNdmSpcLocal("ndmspcZmq2ws");
  const [analysisName, setAnalysisName] = useState(defaultAnalysisName)
  const [mode, setMode] = useState('create')
  const [analysisBaseDirectory, setAnalysisBaseDirectory] = useState('https://eos.ndmspc.io/eos/ndmspc/scratch/ndmspc/demo/phi')
  const [mapFileName, setMapFileName] = useState('https://eos.ndmspc.io/eos/ndmspc/scratch/ndmspc/demo/phi/hMap.root')
  const [mapFileNameExists, setMapFileNameExists] = useState(false)
  const [sourceHistogramFileName, setSourceHistogramFileName] = useState('https://eos.ndmspc.io//eos/ndmspc/scratch/alice/cern.ch/user/a/alihyperloop/outputs/0013/138309/16826/AnalysisResults.root')
  const [sourceHistogramObjectName, setSourceHistogramObjectName] = useState('Unlike')

  const [mapFileNameCurrent, setMapFileNameCurrent] = useState(null)

  // const [mapFileName, setMapFileName] = useState('https://eos0.ndmspc.io//eos/ndmspc/scratch/rsn/test/hMap.root')
  const [object, setObject] = useState(null)
  const [axes, setAxes] = useState([])
  const [availableOptions, setAvailableOptions] = useState([]);
  const [selectedProjection, setSelectedProjection] = useState([]);

  const handleSave = () => {
    const projectionName = selectedProjection.join("_")
    const basefilename = mapFileName.replace("/hMap.root", "")
    const [rootBase, rootPath] = http2root(basefilename)
    const [httpBase] = root2http(rootBase + rootPath)
    let output = null;
    if (ndmspcAnalyses && !isObjectEmpty(ndmspcAnalyses) && ndmspcAnalyses?.analyses) {
      output = { ...ndmspcAnalyses, analysis: analysisName, analyses: { ...ndmspcAnalyses.analyses, [analysisName]: { name: analysisName, description: "", base: { http: httpBase, root: rootBase, path: rootPath }, file: mapFileName, obj: object.fName, projection: projectionName, projections: { ...ndmspcAnalyses?.analyses[analysisName]?.projections, [projectionName]: { name: projectionName, description: "", file: `${basefilename}/${projectionName}/hProjMap.root`, obj: "hProjMap", axes: selectedProjection } } } } }
    } else {
      output = { analysis: analysisName, analyses: { [analysisName]: { name: analysisName, description: "", base: { http: httpBase, root: rootBase, path: rootPath }, file: mapFileName, obj: object.fName, projection: projectionName, projections: { [projectionName]: { name: projectionName, description: "", file: `${basefilename}/${projectionName}/hProjMap.root`, obj: "hProjMap", axes: selectedProjection } } } } }
    }
    ndmspcAnalysesStore.setData(output);
    onClose()
  }


  const handleBaseDirectory = (value) => {
    console.log(value)
    setAnalysisBaseDirectory(value);
    setMapFileName(`${value}/hMap.root`)
    // setImportObjectName(objectname)
  }

  useEffect(() => {
    console.log(mapFileName)

    const fetchData = async (f, exists) => {
      const data = await urlExists(f);
      if (!data) if (exists) setMapFileNameExists(false)
      setMapFileNameExists(data);
    }
    // if (!mapFileNameExists) 
    fetchData(mapFileName, mapFileNameExists).catch(console.error);

  }, [mapFileName]);

  useEffect(() => {
    // console.log("ndmspcAnalyses",ndmspcAnalyses, analysisName)
    if (!ndmspcAnalyses) return
    if (ndmspcAnalyses?.analyses && analysisName.length > 0) {
      // setAnalysisName(ndmspcAnalyses.analysis)
      
      let fn = ndmspcAnalyses.analyses[analysisName].file
      // console.log("AAAAAAAAAAAAAAAAAAAA",fn)
      if (fn) {
        fn  = fn.substring(0, fn.lastIndexOf('/'));
        setMapFileName(ndmspcAnalyses.analyses[analysisName].file)
        setAnalysisBaseDirectory(fn)
        }
      

      
    } 
  }, [ndmspcAnalyses]);


  const handleModeChange = (e) => {
    setMode(e.currentTarget.value)
  }

  const handleFilenameOpen = (filename, isOpen) => {
    console.log("handleFilenameOpen", filename, isOpen)
    setMapFileName(filename)
    if (isOpen) setMapFileNameCurrent(filename)
    else setMapFileNameCurrent(null)

  }

  const handleCreateMapDone = (filename, isOpen) => {
    console.log("handleCreateMapDone", filename, isOpen)
    if (filename) {
      setMapFileName(filename)
      setMapFileNameExists(true)
      setMapFileNameCurrent(null)
      return
    }

    setMapFileName(null)
    setMapFileNameExists(false)
    setMapFileNameCurrent(null)
  }

  const handleObjectSelect = (obj) => {
    if (obj.fName === "hMap") {
      setObject(obj)
      const axisArr = []
      obj.fAxes.arr.forEach(a => {
        axisArr.push(a.fName)
      })
      setAxes(axisArr)
    }
  }


  const onListChange = (event, newAvailableOptions, newChosenOptions) => {
    // setAvailableOptions(newAvailableOptions.sort());
    // setSelectedProjection(newChosenOptions.sort());
    setAvailableOptions(newAvailableOptions);
    setSelectedProjection(newChosenOptions);
  };

  useEffect(() => {
    setAvailableOptions(axes);
    setSelectedProjection([])
  }, [axes]);

  useEffect(() => {
    console.log(mode)
  }, [mode]);



  const modes = [
    {
      label: "New",
      value: "create",
      description: "Create NDM workspace from ROOT histogram. Supported histograms: TH1, TH2, TH3, THn and THnSparse",
      disabled: false
    },
    {
      label: "Import",
      value: "import",
      description: "Import existing NDM workspace"
    },
    // {
    //   label: "Import from config file",
    //   value: "import-config",
    //   description: "Import from NDMSPC config file",
    //   disabled: true
    // }
  ];

  const AnalysisInfoWizardStep = (
    <WizardStep name="Profile" id="analysis-info-step">
      Analysis
      <TextContent>
        <Text component={TextVariants.h3}>Name</Text>
      </TextContent>
      <InputGroup label="Analysis name">
        <InputGroupItem isFill >
          <TextInput
            id='analysisName'
            aria-label='Analysis name'
            value={analysisName}
            onChange={(_event, value) => setAnalysisName(value)}
          />
        </InputGroupItem>
      </InputGroup>
      <br />
      <TextContent>
        <Text component={TextVariants.h3}>Location</Text>
      </TextContent>
      <InputGroup label="Location">
        <InputGroupItem isFill >
          <TextInput
            id='analysisBaseDirectory'
            aria-label='analysis base direcotry'
            value={analysisBaseDirectory}
            onChange={(_event, value) => { handleBaseDirectory(value) }}
          />
        </InputGroupItem>
      </InputGroup>
      <br />
      <TextContent>
        <Text component={TextVariants.h5}>{`${mapFileName}`}
          <Icon status={mapFileNameExists? "success" : "warning"}>
            {mapFileNameExists? <CheckCircleIcon /> : <ExclamationTriangleIcon />}
          </Icon>
        </Text>
      </TextContent>
    </WizardStep>
  )

  const ClusterWizardStep = (
    <WizardStep name="Cluster" id="cluster-step">
      Cluster
      <NdmspcClusterMain />
    </WizardStep>
  )


  // const ModeWizardStep = (
  //   <WizardStep name="Mode" id="mode-step" isHidden={mapFileNameExists}>
  //     <FormGroup label='What action do you take to choose a mode for further continuation?'>
  //       {modes.map((m) =>
  //         <Radio
  //           isChecked={mode === m.value}
  //           isDisabled={m.disabled ?? false}
  //           label={m.label}
  //           id={`mode-${m.value}`}
  //           key={`mode-${m.value}`}
  //           name={m.value}
  //           description={m.description}
  //           value={m.value}
  //           onChange={handleModeChange}
  //         />
  //       )}
  //     </FormGroup>
  //   </WizardStep>
  // )

  const NdmWorkspaceWizardStep = (
    <WizardStep name="Workspace" id="ndmspcpworkspace-step" footer={{ isNextDisabled: (!object || object.fName !== 'hMap') }}>
      Workspace

      {mapFileNameExists ? (
        <>
          <FormGroup label='File Name'>
            <NdmspcFileForm onSubmit={handleFilenameOpen} defaultFileName={mapFileName} disabled={true}/>
          </FormGroup>
          {mapFileNameCurrent ?
            <>
              <br />
              <FormGroup label='Select object'>
                <NdmspcFileBrowser fileName={mapFileNameCurrent} onObjectSelect={handleObjectSelect}/>
              </FormGroup>
            </> : null
          }
        </>
      ) : <CreateMappingFile onSubmit={handleCreateMapDone} defaultFileName={sourceHistogramFileName} output={mapFileName} />

      }

    </WizardStep>
  )

  const ProjectionsWizardStep = (
    <WizardStep name="Projections" id="projections-step" footer={{ isNextDisabled: (selectedProjection.length === 0) }}>
      Projections {selectedProjection.join("_")}
      <DualListSelector availableOptions={availableOptions} chosenOptions={selectedProjection} onListChange={onListChange} id="dual-list-selector-basic" />
    </WizardStep>
  )



  const ReviewWizardStep = (
    <WizardStep name="Review" id="review-step" footer={{
      nextButtonText: 'Finish', isNextDisabled: (analysisName.length === 0)
    }}>

      <br />
      <TextContent>
        <Text component={TextVariants.h3}>Summary</Text>
      </TextContent>
      <DataList aria-label="Summary" isCompact>
        <DataListItem aria-labelledby="summary-filename">
          <DataListItemRow>
            <DataListItemCells dataListCells={[
              <DataListCell key="summary-filename-label">
                <span id="compact-item1">Filename</span>
              </DataListCell>,
              <DataListCell key="summary-filename-value">{mapFileName}
              </DataListCell>]} />
          </DataListItemRow>
        </DataListItem>
        <DataListItem aria-labelledby="summary-object">
          <DataListItemRow>
            <DataListItemCells dataListCells={[
              <DataListCell key="summary-object-label">
                <span id="compact-item1">Object name</span>
              </DataListCell>,
              <DataListCell key="summary-object-value">{object?.fName}
              </DataListCell>]} />
          </DataListItemRow>
        </DataListItem>
        <DataListItem aria-labelledby="summary-projection">
          <DataListItemRow>
            <DataListItemCells dataListCells={[
              <DataListCell key="summary-projection-label">
                <span id="compact-item1">Projection axes</span>
              </DataListCell>,
              <DataListCell key="summary-projection-value">{selectedProjection.join(",")}
              </DataListCell>]} />
          </DataListItemRow>
        </DataListItem>

        <DataListItem aria-labelledby="summary-api">
          <DataListItemRow>
            <DataListItemCells dataListCells={[
              <DataListCell key="summary-api-label"><span id="compact-item1">NDMSPC api server</span></DataListCell>,
              <DataListCell key="summary-api-value">{ndmspcZmq2ws?.url}</DataListCell>]} />
          </DataListItemRow>
        </DataListItem>

        <DataListItem aria-labelledby="summary-site">
          <DataListItemRow>
            <DataListItemCells dataListCells={[
              <DataListCell key="summary-site-label"><span id="compact-item1">Site</span></DataListCell>,
              <DataListCell key="summary-site-value">{ndmspcApp?.sub?.split(":")[0]}</DataListCell>]} />
          </DataListItemRow>
        </DataListItem>
        <DataListItem aria-labelledby="summary-cluster">
          <DataListItemRow>
            <DataListItemCells dataListCells={[
              <DataListCell key="summary-cluster-label"><span id="compact-item1">Cluster</span></DataListCell>,
              <DataListCell key="summary-cluster-value">{ndmspcApp?.sub?.split(":")[1]}</DataListCell>]} />
          </DataListItemRow>
        </DataListItem>
      </DataList>
    </WizardStep>
  )

  return (
    <Wizard height={500} title="Basic wizard" isVisitRequired onClose={onClose} onSave={handleSave}>
      {AnalysisInfoWizardStep}
      {ClusterWizardStep}
      {/* {ModeWizardStep} */}
      {NdmWorkspaceWizardStep}
      {ProjectionsWizardStep}

      {ReviewWizardStep}
    </Wizard>
  );
};